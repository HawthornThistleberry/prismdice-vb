VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{C1A8AF28-1257-101B-8FB0-0020AF039CA3}#1.1#0"; "MCI32.OCX"
Begin VB.Form frmDiceBar 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Prism Dice"
   ClientHeight    =   5655
   ClientLeft      =   7395
   ClientTop       =   1800
   ClientWidth     =   3255
   HelpContextID   =   100
   Icon            =   "Dice.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   NegotiateMenus  =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   377
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   217
   Begin VB.TextBox txtRollsLog 
      BackColor       =   &H80000016&
      Height          =   5655
      HideSelection   =   0   'False
      Left            =   3360
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   20
      TabStop         =   0   'False
      Tag             =   "0"
      Top             =   0
      Visible         =   0   'False
      Width           =   2775
   End
   Begin VB.CommandButton cmdDiceSettings 
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   15.75
         Charset         =   2
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      HelpContextID   =   200
      Index           =   7
      Left            =   2760
      TabIndex        =   18
      Top             =   5160
      Width           =   495
   End
   Begin VB.CommandButton cmdDiceSettings 
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   15.75
         Charset         =   2
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      HelpContextID   =   200
      Index           =   6
      Left            =   2760
      TabIndex        =   16
      Top             =   4680
      Width           =   495
   End
   Begin VB.CommandButton cmdDiceSettings 
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   15.75
         Charset         =   2
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      HelpContextID   =   200
      Index           =   5
      Left            =   2760
      TabIndex        =   14
      Top             =   4200
      Width           =   495
   End
   Begin VB.CommandButton cmdDiceSettings 
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   15.75
         Charset         =   2
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      HelpContextID   =   200
      Index           =   4
      Left            =   2760
      TabIndex        =   12
      Top             =   3720
      Width           =   495
   End
   Begin VB.CommandButton cmdDiceSettings 
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   15.75
         Charset         =   2
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      HelpContextID   =   200
      Index           =   3
      Left            =   2760
      TabIndex        =   10
      Top             =   3240
      Width           =   495
   End
   Begin VB.CommandButton cmdDiceSettings 
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   15.75
         Charset         =   2
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      HelpContextID   =   200
      Index           =   2
      Left            =   2760
      TabIndex        =   8
      Top             =   2760
      Width           =   495
   End
   Begin VB.CommandButton cmdDiceSettings 
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   15.75
         Charset         =   2
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      HelpContextID   =   200
      Index           =   1
      Left            =   2760
      TabIndex        =   6
      Top             =   2280
      Width           =   495
   End
   Begin VB.CommandButton cmdDice 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      HelpContextID   =   300
      Index           =   7
      Left            =   0
      TabIndex        =   17
      Top             =   5160
      Width           =   2775
   End
   Begin VB.CommandButton cmdDice 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      HelpContextID   =   300
      Index           =   6
      Left            =   0
      TabIndex        =   15
      Top             =   4680
      Width           =   2775
   End
   Begin VB.CommandButton cmdDice 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      HelpContextID   =   300
      Index           =   5
      Left            =   0
      TabIndex        =   13
      Top             =   4200
      Width           =   2775
   End
   Begin VB.CommandButton cmdDice 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      HelpContextID   =   300
      Index           =   4
      Left            =   0
      TabIndex        =   11
      Top             =   3720
      Width           =   2775
   End
   Begin VB.CommandButton cmdDice 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      HelpContextID   =   300
      Index           =   3
      Left            =   0
      TabIndex        =   9
      Top             =   3240
      Width           =   2775
   End
   Begin VB.CommandButton cmdDice 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      HelpContextID   =   300
      Index           =   2
      Left            =   0
      TabIndex        =   7
      Top             =   2760
      Width           =   2775
   End
   Begin VB.CommandButton cmdDice 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      HelpContextID   =   300
      Index           =   1
      Left            =   0
      TabIndex        =   5
      Top             =   2280
      Width           =   2775
   End
   Begin VB.CommandButton cmdDiceSettings 
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   15.75
         Charset         =   2
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      HelpContextID   =   200
      Index           =   0
      Left            =   2760
      TabIndex        =   4
      Top             =   1800
      Width           =   495
   End
   Begin VB.ComboBox cmbPages 
      Height          =   315
      HelpContextID   =   400
      ItemData        =   "Dice.frx":030A
      Left            =   0
      List            =   "Dice.frx":0311
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   1440
      Width           =   3255
   End
   Begin VB.CommandButton cmdDice 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      HelpContextID   =   300
      Index           =   0
      Left            =   0
      TabIndex        =   3
      Top             =   1800
      Width           =   2775
   End
   Begin MCI.MMControl mciControl 
      Height          =   330
      Left            =   0
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      Width           =   3540
      _ExtentX        =   6244
      _ExtentY        =   582
      _Version        =   393216
      PlayEnabled     =   -1  'True
      DeviceType      =   "WaveAudio"
      FileName        =   "C:\Program Files\Prism Dice\dice.wav"
   End
   Begin MSComDlg.CommonDialog cdlGeneric 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      HelpContext     =   100
   End
   Begin VB.Label lblDetails 
      Alignment       =   2  'Center
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   0
      TabIndex        =   1
      Top             =   720
      Width           =   3255
   End
   Begin VB.Label lblResult 
      Alignment       =   2  'Center
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   615
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3255
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      HelpContextID   =   500
      Begin VB.Menu mnuManagePages 
         Caption         =   "Manage &Pages..."
         HelpContextID   =   400
      End
      Begin VB.Menu mnuSep0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuImportSettings 
         Caption         =   "&Import Settings"
         HelpContextID   =   500
         Shortcut        =   ^I
      End
      Begin VB.Menu mnuExportSettings 
         Caption         =   "&Export Settings"
         HelpContextID   =   500
         Shortcut        =   ^E
      End
      Begin VB.Menu mnuSep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
         Shortcut        =   ^X
      End
   End
   Begin VB.Menu mnuOptions 
      Caption         =   "&Options"
      HelpContextID   =   300
      Begin VB.Menu mnuOptionsSound 
         Caption         =   "&Sound"
         Checked         =   -1  'True
         HelpContextID   =   300
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuOptionsShowIndividualRolls 
         Caption         =   "Show Individual &Rolls"
         Checked         =   -1  'True
         HelpContextID   =   300
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuOptionsShowRollsLog 
         Caption         =   "Show Rolls &Log"
         Shortcut        =   ^L
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      HelpContextID   =   100
      Begin VB.Menu mnuContents 
         Caption         =   "&Contents"
         HelpContextID   =   100
      End
      Begin VB.Menu mnuAbout 
         Caption         =   "&About"
         HelpContextID   =   100
         Shortcut        =   ^A
      End
   End
End
Attribute VB_Name = "frmDiceBar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim Pages As New Collection
Public ptrPages As Object
Public lastPageIndex As Integer
Dim lastButton As Integer
Dim CurrentPage As Object

Private Sub Form_Load()
  Dim i As Integer, numpages As Integer
  Dim pagename As String, curpagename As String
  Set ptrPages = Pages ' to make it available in other forms
  ' load settings
  Call SaveSetting(conAppName, "Startup", "Path", App.Path & "\" & App.EXEName & ".exe")
  Left = GetSetting(conAppName, "Startup", "Left", (Screen.Width - Width) / 2)
  Top = GetSetting(conAppName, "Startup", "Top", (Screen.Height - Height) / 2)
  ' set up the sound to play
  mciControl.Notify = False
  mciControl.Wait = True
  mciControl.Shareable = False
  mciControl.DeviceType = "WaveAudio"
  mciControl.FileName = App.Path & "\dice.wav"
  mciControl.Command = "Open"
  If GetSetting(conAppName, "Startup", "ShowIndividualRolls", True) = False Then
      ' let the function do the work
      mnuOptionsShowIndividualRolls_Click
      ' no need for an else since everything's already right if it's checked
    End If
  If GetSetting(conAppName, "Startup", "ShowRollsLog", False) = True Then
      ' let the function do the work
      mnuOptionsShowRollsLog_Click
      ' no need for an else since everything's already right if it's unchecked
    End If
  If GetSetting(conAppName, "Startup", "Sound", True) = False Then
      ' let the function do the work
      mnuOptionsSound_Click
      ' no need for an else since everything's already right if it's checked
    End If
  ' are there saved pages?
  numpages = GetSetting(conAppName, "Startup", "NumPages", 0)
  cmbPages.Clear
  If numpages = 0 Then ' create a default page
      Dim DefaultPage As New clsPage
      DefaultPage.Create
      Pages.Add DefaultPage, DefaultPage.Label
      Set CurrentPage = DefaultPage
      cmbPages.AddItem DefaultPage.Label
    Else ' load the saved pages
      curpagename = GetSetting(conAppName, "Startup", "CurrentPage", "")
      For i = 1 To numpages
        pagename = GetSetting(conAppName, "Startup", "Page" & Trim(Str(i)), "")
        If pagename <> "" Then
            Dim Page As New clsPage
            Page.Create
            Page.LoadFromRegistry pagename
            If curpagename = pagename Or i = 1 Then Set CurrentPage = Page
            Pages.Add Page, Page.Label
            cmbPages.AddItem Page.Label
            Set Page = Nothing
          End If
        Next
    End If
  cmbPages.AddItem "Manage Pages..."
  ' set the current page from the CurrentPage key
  CurrentPage.ShowPage
  lastButton = 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
  Dim i As Integer, Page As Object
  Unload frmEditPages
  Unload frmDiceSettings
  Unload frmAbout
  mciControl.Command = "Close"
  ' save settings
  Call SaveSetting(conAppName, "Startup", "Left", Left)
  Call SaveSetting(conAppName, "Startup", "Top", Top)
  Call SaveSetting(conAppName, "Startup", "ShowIndividualRolls", mnuOptionsShowIndividualRolls.Checked)
  Call SaveSetting(conAppName, "Startup", "ShowRollsLog", mnuOptionsShowRollsLog.Checked)
  ' Save pages
  For i = 1 To cmbPages.ListCount - 1
    Set Page = FindPage(cmbPages.List(i - 1))
    Page.SaveToRegistry
    Call SaveSetting(conAppName, "Startup", "Page" & Trim(Str(i)), Page.Label)
    Next
  ' Save current page pointer
  Call SaveSetting(conAppName, "Startup", "NumPages", Pages.Count)
  Call SaveSetting(conAppName, "Startup", "CurrentPage", CurrentPage.Label)
End Sub

Public Function FindPage(name As String) As Object
  Dim Page As Object
  For Each Page In ptrPages
    If Page.Label = name Then
        Set FindPage = Page
        Exit Function
      End If
    Next
  Set FindPage = Nothing
End Function
 

Private Sub mnuAbout_Click()
  frmAbout.Show 1
End Sub

Private Sub mnuContents_Click()
  cdlGeneric.HelpFile = App.Path & "\Dice.hlp"
  cdlGeneric.HelpCommand = cdlHelpContents
  cdlGeneric.action = 6
End Sub

Private Sub mnuFileExit_Click()
  Unload Me
End Sub

Private Sub cmbPages_Click()
  If cmbPages.ListIndex = cmbPages.ListCount - 1 Then
      ' bring up the list change form
      frmEditPages.Show 1
      ' jump to that page
      If frmEditPages.lisPageList.ListIndex <> -1 Then
          cmbPages.ListIndex = frmEditPages.lisPageList.ListIndex
        Else
          cmbPages.ListIndex = lastPageIndex
        End If
      ' the above will recurse this routine which will change pages for us
      Exit Sub
    End If
  ' change pages
  Set CurrentPage = FindPage(cmbPages.List(cmbPages.ListIndex))
  If CurrentPage Is Nothing Then
      MsgBox "Error: page not found!", vbOKOnly + vbCritical, "Prism Dice"
      Exit Sub
    End If
  CurrentPage.ShowPage
  lastPageIndex = cmbPages.ListIndex
End Sub

Private Sub cmdDice_Click(Index As Integer)
  Dim Roll As Integer, details As String, i As Integer
  lastButton = Index
  lblResult = CurrentPage.Buttons(Index + 1).Roll(details)
  lblDetails = details
  If lblResult.ForeColor = &HFF& Then ' back color &HE0E0E0
      lblResult.ForeColor = &HFF0000
    Else
      lblResult.ForeColor = &HFF&
    End If
  lblResult.BackColor = &HE0E0E0
  lblDetails.BackColor = &HE0E0E0
  If mnuOptionsSound.Checked Then
      mciControl.Command = "Play"
      mciControl.Command = "Prev"
    End If
  ' if the log is at its maximum size,
  If txtRollsLog.Tag = MAXIMUM_LOG_LINES Then
      ' trim one line
      i = InStr(txtRollsLog, vbCrLf)
      If i = 0 Then
          txtRollsLog = ""
          txtRollsLog.Tag = 0
        Else
          txtRollsLog = Mid(txtRollsLog, i + 2)
        End If
    Else
      txtRollsLog.Tag = txtRollsLog.Tag + 1
    End If
  ' add to the log
  If txtRollsLog <> "" Then txtRollsLog = txtRollsLog & vbCrLf
  i = Len(txtRollsLog)
  txtRollsLog = txtRollsLog & cmdDice(Index).Caption & ": " & lblResult & " (" & lblDetails & ")"
  txtRollsLog.SelStart = i
  txtRollsLog.SelLength = Len(txtRollsLog) - i
End Sub

Private Sub cmdDiceSettings_Click(Index As Integer)
  Dim i As Integer
  ' load the form's values
  frmDiceSettings.txtLabel = CurrentPage.Buttons(Index + 1).Label
  frmDiceSettings.txtNumDice = CurrentPage.Buttons(Index + 1).NumDice
  frmDiceSettings.cmbDieSize = CurrentPage.Buttons(Index + 1).DieSize
  frmDiceSettings.txtDieParam = CurrentPage.Buttons(Index + 1).DieParam
  frmDiceSettings.txtDieAdder = CurrentPage.Buttons(Index + 1).DieAdder
  For i = 1 To frmDiceSettings.cmbDieType.ListCount
    If frmDiceSettings.cmbDieType.ItemData(i - 1) = CurrentPage.Buttons(Index + 1).DieType Then
        frmDiceSettings.cmbDieType.ListIndex = i - 1
        Exit For
      End If
    Next
  frmDiceSettings.txtTotalAdder = CurrentPage.Buttons(Index + 1).TotalAdder
  frmDiceSettings.txtTotalMultiplier = CurrentPage.Buttons(Index + 1).TotalMultiplier
  frmDiceSettings.optSortNone = False
  frmDiceSettings.optSortAscending = False
  frmDiceSettings.optSortDescending = False
  Select Case CurrentPage.Buttons(Index + 1).sorttype
    Case SORT_NONE:       frmDiceSettings.optSortNone = True
    Case SORT_ASCENDING:  frmDiceSettings.optSortAscending = True
    Case SORT_DESCENDING: frmDiceSettings.optSortDescending = True
    End Select
  frmDiceSettings.Caption = "Dice Settings for " & _
    cmbPages.List(cmbPages.ListIndex) & " button #" & CStr(Index + 1) & _
    " (" & CurrentPage.Buttons(Index + 1).Label & ")"
  frmDiceSettings.Show 1
  If frmDiceSettings.Tag = "1" Then
      CurrentPage.Buttons(Index + 1).Label = frmDiceSettings.txtLabel
      CurrentPage.Buttons(Index + 1).NumDice = frmDiceSettings.txtNumDice
      CurrentPage.Buttons(Index + 1).DieSize = frmDiceSettings.cmbDieSize
      CurrentPage.Buttons(Index + 1).DieType = frmDiceSettings.cmbDieType.ItemData(frmDiceSettings.cmbDieType.ListIndex)
      CurrentPage.Buttons(Index + 1).DieParam = frmDiceSettings.txtDieParam
      CurrentPage.Buttons(Index + 1).DieAdder = frmDiceSettings.txtDieAdder
      CurrentPage.Buttons(Index + 1).TotalAdder = frmDiceSettings.txtTotalAdder
      CurrentPage.Buttons(Index + 1).TotalMultiplier = frmDiceSettings.txtTotalMultiplier
      CurrentPage.Buttons(Index + 1).sorttype = IIf(frmDiceSettings.optSortNone, SORT_NONE, IIf(frmDiceSettings.optSortAscending, SORT_ASCENDING, SORT_DESCENDING))
      cmdDice(Index).Caption = DoubleAmpersands(CurrentPage.Buttons(Index + 1).Label)
    End If
End Sub

Private Sub lblResult_Click()
  Clipboard.SetText lblResult, vbCFText
  lblResult.BackColor = &HC0C0C0
  lblDetails.BackColor = &HE0E0E0
End Sub

Private Sub lblResult_DblClick()
  cmdDice_Click lastButton
End Sub

Private Sub lblDetails_Click()
  Clipboard.SetText lblDetails, vbCFText
  lblDetails.BackColor = &HC0C0C0
  lblResult.BackColor = &HE0E0E0
End Sub

Private Sub lblDetails_DblClick()
  cmdDice_Click lastButton
End Sub

Private Sub mnuManagePages_Click()
  ' bring up the list change form
  frmEditPages.Show 1
  ' jump to that page
  If frmEditPages.lisPageList.ListIndex <> -1 Then
      cmbPages.ListIndex = frmEditPages.lisPageList.ListIndex
    Else
      cmbPages.ListIndex = lastPageIndex
    End If
End Sub

Private Sub mnuOptionsSound_Click()
  Dim i As Integer
  If mnuOptionsSound.Checked Then ' uncheck
      mnuOptionsSound.Checked = False
    Else ' check
      mnuOptionsSound.Checked = True
    End If
End Sub

Private Sub mnuOptionsShowIndividualRolls_Click()
  Dim i As Integer
  If mnuOptionsShowIndividualRolls.Checked Then ' uncheck
      mnuOptionsShowIndividualRolls.Checked = False
      ' hide the details field
      lblDetails.Visible = False
      ' move everything up 48 pixels
      cmbPages.Top = cmbPages.Top - 48
      For i = 0 To 7
        cmdDice(i).Top = cmdDice(i).Top - 48
        cmdDiceSettings(i).Top = cmdDiceSettings(i).Top - 48
        Next i
      ' resize the log field
      txtRollsLog.Height = txtRollsLog.Height - 48
      ' resize the form by 48 pixels
      Height = Height - 48 * Screen.TwipsPerPixelY
    Else ' check
      mnuOptionsShowIndividualRolls.Checked = True
      ' resize the form up by 48
      Height = Height + 48 * Screen.TwipsPerPixelY
      ' move everything down 48 pixels
      For i = 7 To 0 Step -1
        cmdDice(i).Top = cmdDice(i).Top + 48
        cmdDiceSettings(i).Top = cmdDiceSettings(i).Top + 48
        Next i
      cmbPages.Top = cmbPages.Top + 48
      ' resize the log field
      txtRollsLog.Height = txtRollsLog.Height + 48
      ' show the details field
      lblDetails.Visible = True
    End If
End Sub

Private Sub mnuOptionsShowRollsLog_Click()
  Dim i As Integer
  If mnuOptionsShowRollsLog.Checked Then ' uncheck
      mnuOptionsShowRollsLog.Checked = False
      ' hide the log field
      txtRollsLog.Visible = False
      ' resize the form
      Width = 3345
    Else ' check
      mnuOptionsShowRollsLog.Checked = True
      ' resize the form
      Width = 6240
      ' show the log field
      txtRollsLog.Visible = True
    End If
End Sub

Public Function DoubleAmpersands(s As String) As String
  Dim r As String
  Dim i As Integer
  r = s
  i = InStr(s, "&")
  While i <> 0
    r = Left(r, i) & "&" & Mid(r, i + 1)
    i = InStr(i + 2, s, "&")
    Wend
  DoubleAmpersands = r
End Function

Private Sub Form_KeyPress(KeyAscii As Integer)
  Dim KeyPressed As String
  KeyPressed = UCase$(Chr$(KeyAscii))
  Select Case KeyPressed
    Case "1", "2", "3", "4", "5", "6", "7", "8"
      cmdDice_Click (Val(KeyPressed) - 1)
    Case "P"
      cmbPages.ListIndex = cmbPages.ListCount - 1
    Case " "
        cmdDice_Click lastButton
    End Select
  KeyAscii = 0
End Sub

Private Sub mnuExportSettings_Click()
  Dim errorText As String
  Dim i As Integer, Page As Object
  ' set up the common dialog for a save
  cdlGeneric.DialogTitle = "Export dice settings"
  cdlGeneric.FileName = "PrismDice"
  cdlGeneric.DefaultExt = "*.prism"
  cdlGeneric.Filter = "Prism Files (*.prism)|*.prism|Text Files (*.txt)|*.txt|All Files (*.*)|*.*"
  cdlGeneric.FilterIndex = 1
  cdlGeneric.Flags = cdlOFNHideReadOnly + cdlOFNPathMustExist + cdlOFNExplorer + cdlOFNLongNames + cdlOFNOverwritePrompt
  cdlGeneric.CancelError = True
  ' launch the dialog
  On Error GoTo cancelled
  cdlGeneric.ShowSave
  If cdlGeneric.FileName = "" Then Exit Sub

  ' open the file (for rewrite)
  On Error GoTo fileError
  errorText = " opening file"
  Open cdlGeneric.FileName For Output As #1
  errorText = " writing to file"
  Write #1, "PrismTools - " & conAppName & " Settings v1.0"
  Write #1, "NumPages", Pages.Count
  Write #1, "CurrentPage", CurrentPage.Label
  For i = 1 To cmbPages.ListCount - 1
    Write #1, "Page", i, cmbPages.List(i - 1)
    Set Page = FindPage(cmbPages.List(i - 1))
    Page.Export 1
    Next
  ' close the file
  errorText = " closing file"
  Close #1
  Exit Sub
' error message handlers
fileError:
  Close #1
  MsgBox "Error" & errorText & " " & cdlGeneric.FileName, vbCritical, "Prism Dice"
cancelled:
  Exit Sub
End Sub

Private Sub mnuImportSettings_Click()
  Dim errorText As String, s As String, Label As String, curpagename As String
  Dim i As Integer, j As Integer, numpages As Integer
  
  If MsgBox("Really import over current settings?", vbYesNo + vbQuestion, conAppName) = vbNo Then Exit Sub
  
  ' set up the common dialog for a load
  cdlGeneric.DialogTitle = "Load settings"
  cdlGeneric.FileName = "PrismDice"
  cdlGeneric.DefaultExt = "*.prism"
  cdlGeneric.Filter = "Prism Files (*.prism)|*.prism|Text Files (*.txt)|*.txt|All Files (*.*)|*.*"
  cdlGeneric.FilterIndex = 1
  cdlGeneric.Flags = cdlOFNFileMustExist + cdlOFNHideReadOnly + _
      cdlOFNPathMustExist + cdlOFNExplorer + cdlOFNLongNames
  cdlGeneric.CancelError = True
  On Error GoTo cancelled
  cdlGeneric.ShowOpen
  If cdlGeneric.FileName = "" Then Exit Sub

  ' open the file (for rewrite)
  On Error GoTo fileError
  errorText = " opening file"
  Open cdlGeneric.FileName For Input As #1
  errorText = " reading header"
  Input #1, s
  If s <> "PrismTools - " & conAppName & " Settings v1.0" Then
      MsgBox "Error: file is of incorrect format.", vbExclamation, "Prism Dice"
      Close #1
      Exit Sub
    End If
  
  ' wipe out the current settings
  errorText = " erasing old settings"
  Do
    Pages.Remove 1
    Loop While Pages.Count > 0
  Set CurrentPage = Nothing
  cmbPages.Clear
  
  ' read heading information
  errorText = " reading summary"
  Input #1, s, numpages
  Input #1, s, curpagename
  For i = 1 To numpages
    errorText = " reading page" & Str(i)
    Input #1, s, j, Label
    ' create a new page
    Dim Page As New clsPage
    errorText = " creating page" & Str(i)
    Page.Create
    Page.Label = Label
    Pages.Add Page, Label
    ' load the page
    errorText = " loading page" & Str(i)
    Page.Import 1
    errorText = " checking page" & Str(i) & " for currency"
    If curpagename = Label Or i = 1 Then Set CurrentPage = Page
    errorText = " freeing page pointer"
    Set Page = Nothing
    ' add the label to cmbPages
    errorText = " adding page" & Str(i) & " to menu"
    cmbPages.AddItem Label
    Next
  cmbPages.AddItem "Manage Pages..."
  ' close the file
  errorText = " closing file"
  Close #1
  errorText = " setting current page"
  CurrentPage.ShowPage
  lastButton = 0
  Exit Sub
' error message handlers
fileError:
  Close #1
  MsgBox "Error" & errorText & " " & cdlGeneric.FileName, vbCritical, "Prism Dice"
cancelled:
  Exit Sub
End Sub

Private Sub txtRollsLog_DblClick()
  txtRollsLog = ""
  txtRollsLog.Tag = 0
End Sub
