Attribute VB_Name = "clsDiceSettings"
Option Explicit

' This file contains the data structures for the
' dice settings data that drives the whole program.

Const DICE_TOTAL = 0          ' normal dice (P=ignored)
Const DICE_TOTAL_HIGHEST = 10 ' total the highest rolls (P=how many)
Const DICE_TOTAL_LOWEST = 11  ' total the lowest rolls (P=how many)
Const DICE_DIGITAL = 20       ' read off results as digits (P=0 or 1 for base)
Const DICE_OPEN_ENDED = 30    ' Rolemaster-style dice (P=size of reroll range)
Const DICE_OPEN_HIGH = 31     ' as above but high-open-ended only
Const DICE_OPEN_LOW = 32      ' as above but low-open-ended only
Const DICE_COUNT_UNDER = 40   ' count rolls under a value (P=the value)
Const DICE_COUNT_OVER = 41    ' count rolls over a value (P=the value)
Const DICE_TOTAL_UNDER = 50   ' total rolls under a value (P=the value)
Const DICE_TOTAL_OVER = 51    ' total rolls over a value (P=the value)
Const DICE_AVERAGING = 60     ' averaging dice (P=how tightly averaged)
Const DICE_FUDGE = 61         ' FUDGE-style dice (P=size of minus/plus range)
Const DICE_SERIES_COUNT = 70  ' count rolls until failure (P=roll under value)
Const DICE_DIVIDED = 80       ' divide one die roll by another (P=denominator size)

Const SORT_NONE = 0
Const SORT_ASCENDING = 1
Const SORT_DESCENDING = 2

Public Type DiceSettings
  Label As String             ' text to appear on die
  NumDice As Integer          ' roll this many dice
  DieType As Integer          ' see constants above
  DieSize As Integer          ' number of sides of dice
  DieParam As Integer         ' parameter P mentioned above
  DieAdder As Integer         ' add to each die
  TotalAdder As Integer       ' add to total
  DieMultiplier As Integer    ' multiply each die by this
  TotalMultiplier As Integer  ' multiply total by this
  SortType As Integer         ' should individual results be sorted?
End Type

Public Type DicePage
  PageLabel As String         ' name for the page
  Buttons(8) As DiceSettings  ' the actual dice settings for the buttons
End Type

' how will we handle an arbitrary number of pages?
'Public Pages As Collection

' functions to:
'   load a page from the registry
'   save a page to the registry
'   create a new page
'   delete a page?

Public Sub MakeDefaultPage(Page As DicePage)
  Page.PageLabel = "Default Page"
  MakeDefaultButton Page.Buttons(1), 4
  MakeDefaultButton Page.Buttons(2), 6
  MakeDefaultButton Page.Buttons(3), 8
  MakeDefaultButton Page.Buttons(4), 10
  MakeDefaultButton Page.Buttons(5), 12
  MakeDefaultButton Page.Buttons(6), 20
  MakeDefaultButton Page.Buttons(7), 30
  MakeDefaultButton Page.Buttons(8), 100
End Sub

Public Sub MakeDefaultButton(Button As DiceSettings, Size As Integer)
  If Size = 100 Then
      Button.Label = "d%"
    Else
      Button.Label = "d" & Trim(Str(Size))
    End If
  Button.NumDice = 1
  Button.DieType = DICE_TOTAL
  Button.DieSize = Size
  Button.DieParam = 0
  Button.DieAdder = 0
  Button.TotalAdder = 0
  Button.DieMultiplier = 1
  Button.TotalMultiplier = 1
  Button.SortType = SORT_NONE
End Sub

Public Sub SetButtonsToPage(Page As DicePage)
  Dim i As Integer
  '-- make sure the combo box points to the right one
  For i = 1 To 8
    frmDiceBar.cmdDice(i - 1).Caption = Page.Buttons(i).Label
    Next
End Sub

' registry: Startup has Left, Top, NumPages, and the page names as Page1, Page2, etc.
'   each page is in a section with its name (thus names must be unique) and contains
'   1Label, 1NumDice, 1DieType, 1DieSize, 1DieParam, etc. for #1
'   repeat for 2 and up

Public Sub LoadPageFromRegistry(Page As DicePage, Label As String)
  Dim i As Integer
  Page.Label = Label
  For i = 1 To 8
    Page.Buttons(i).NumDice = GetSetting(conAppName, Label, Trim(Str(i)) & "NumDice", 1)
    Page.Buttons(i).DieType = GetSetting(conAppName, Label, Trim(Str(i)) & "DieType", DICE_TOTAL)
    Page.Buttons(i).DieSize = GetSetting(conAppName, Label, Trim(Str(i)) & "DieSize", 6)
    Page.Buttons(i).DieParam = GetSetting(conAppName, Label, Trim(Str(i)) & "DieParam", 0)
    Page.Buttons(i).DieAdder = GetSetting(conAppName, Label, Trim(Str(i)) & "DieAdder", 0)
    Page.Buttons(i).TotalAdder = GetSetting(conAppName, Label, Trim(Str(i)) & "TotalAdder", 0)
    Page.Buttons(i).DieMultiplier = GetSetting(conAppName, Label, Trim(Str(i)) & "DieMultiplier", 1)
    Page.Buttons(i).TotalMultiplier = GetSetting(conAppName, Label, Trim(Str(i)) & "TotalMultiplier", 1)
    Page.Buttons(i).SortType = GetSetting(conAppName, Label, Trim(Str(i)) & "SortType", SORT_NONE)
    Next
End Sub

Public Sub SavePageToRegistry(Page As DicePage)
  Dim i As Integer
  For i = 1 To 8
    Call SaveSetting(conAppName, Page.Label, Trim(Str(i)) & "NumDice", Page.Buttons(i).NumDice)
    Call SaveSetting(conAppName, Page.Label, Trim(Str(i)) & "DieType", Page.Buttons(i).DieType)
    Call SaveSetting(conAppName, Page.Label, Trim(Str(i)) & "DieSize", Page.Buttons(i).DieSize)
    Call SaveSetting(conAppName, Page.Label, Trim(Str(i)) & "DieParam", Page.Buttons(i).DieParam)
    Call SaveSetting(conAppName, Page.Label, Trim(Str(i)) & "DieAdder", Page.Buttons(i).DieAdder)
    Call SaveSetting(conAppName, Page.Label, Trim(Str(i)) & "TotalAdder", Page.Buttons(i).TotalAdder)
    Call SaveSetting(conAppName, Page.Label, Trim(Str(i)) & "DieMultiplier", Page.Buttons(i).DieMultiplier)
    Call SaveSetting(conAppName, Page.Label, Trim(Str(i)) & "TotalMultiplier", Page.Buttons(i).TotalMultiplier)
    Call SaveSetting(conAppName, Page.Label, Trim(Str(i)) & "SortType", Page.Buttons(i).SortType)
    Next
End Sub

