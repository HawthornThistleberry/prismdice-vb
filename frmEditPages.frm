VERSION 5.00
Begin VB.Form frmEditPages 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Manage Pages"
   ClientHeight    =   3195
   ClientLeft      =   2325
   ClientTop       =   1620
   ClientWidth     =   5445
   HelpContextID   =   400
   Icon            =   "frmEditPages.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3195
   ScaleWidth      =   5445
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdClose 
      Cancel          =   -1  'True
      Caption         =   "Close"
      Default         =   -1  'True
      Height          =   495
      HelpContextID   =   400
      Left            =   3960
      TabIndex        =   6
      Top             =   2640
      Width           =   1455
   End
   Begin VB.CommandButton cmdDown 
      Caption         =   "Move Down"
      Height          =   495
      HelpContextID   =   400
      Left            =   3960
      TabIndex        =   5
      Top             =   2040
      Width           =   1455
   End
   Begin VB.CommandButton cmdUp 
      Caption         =   "Move Up"
      Height          =   495
      HelpContextID   =   400
      Left            =   3960
      TabIndex        =   4
      Top             =   1560
      Width           =   1455
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "Delete"
      Height          =   495
      HelpContextID   =   400
      Left            =   3960
      TabIndex        =   3
      Top             =   960
      Width           =   1455
   End
   Begin VB.CommandButton cmdRename 
      Caption         =   "Rename"
      Height          =   495
      HelpContextID   =   400
      Left            =   3960
      TabIndex        =   2
      Top             =   480
      Width           =   1455
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "Add"
      Height          =   495
      HelpContextID   =   400
      Left            =   3960
      TabIndex        =   1
      Top             =   0
      Width           =   1455
   End
   Begin VB.ListBox lisPageList 
      Height          =   3180
      HelpContextID   =   400
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3855
   End
End
Attribute VB_Name = "frmEditPages"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
  ' center the form on the screen
  Left = (Screen.Width - Width) / 2
  Top = (Screen.Height - Height) / 2
End Sub

Private Sub Form_Activate()
  Dim i As Integer
  ' load the page list
  lisPageList.Clear
  For i = 0 To frmDiceBar.cmbPages.ListCount - 2
    lisPageList.AddItem frmDiceBar.cmbPages.List(i)
    Next
  Call frmEditPages_EnableDisable
  lisPageList.SetFocus
End Sub

Private Sub frmEditPages_EnableDisable()
  If lisPageList.ListIndex = -1 Then
      cmdRename.Enabled = False
      cmdDelete.Enabled = False
      cmdUp.Enabled = False
      cmdDown.Enabled = False
    Else
      cmdRename.Enabled = True
      If lisPageList.ListCount > 1 Then
          cmdDelete.Enabled = True
          cmdUp.Enabled = True
          cmdDown.Enabled = True
        Else
          cmdDelete.Enabled = False
          cmdUp.Enabled = False
          cmdDown.Enabled = False
        End If
      If lisPageList.ListIndex = 0 Then cmdUp.Enabled = False
      If lisPageList.ListIndex = lisPageList.ListCount - 1 Then cmdDown.Enabled = False
    End If
End Sub

Private Sub lisPageList_Click()
  Call frmEditPages_EnableDisable
End Sub

Private Sub cmdAdd_Click()
  Dim name As String
  ' get a new, unique page name
  name = PromptForName("Enter a name for the new page:", "")
  If name = "" Then Exit Sub
  ' create a new page
  Dim NewPage As New clsPage
  NewPage.Create
  NewPage.Label = name
  frmDiceBar.ptrPages.Add NewPage, NewPage.Label
  frmDiceBar.cmbPages.AddItem NewPage.Label, frmDiceBar.cmbPages.ListCount - 1
  ' add it to the list
  lisPageList.AddItem NewPage.Label
  Call frmEditPages_EnableDisable
End Sub

Private Sub cmdRename_Click()
  Dim name As String
  Dim Page As Object
  ' find the page in the collection
  Set Page = frmDiceBar.FindPage(lisPageList.List(lisPageList.ListIndex))
  If Page Is Nothing Then
      MsgBox "Error: page not found!", vbOKOnly + vbCritical, "Manage Pages"
      Exit Sub
    End If
  ' get a new, unique page name
  name = PromptForName("Enter the new name for """ & lisPageList.List(lisPageList.ListIndex) & """:", Page.Label)
  If name = "" Then Exit Sub
  ' change it in the collection
  frmDiceBar.ptrPages.Remove Page.Label
  Page.Label = name
  frmDiceBar.ptrPages.Add Page, Page.Label
  ' change it in cmbPages
  frmDiceBar.cmbPages.List(lisPageList.ListIndex) = name
  ' change it in lisPageList
  lisPageList.List(lisPageList.ListIndex) = name
  Call frmEditPages_EnableDisable
End Sub

Private Sub cmdDelete_Click()
  Dim name As String
  Dim Page As Object
  ' find the page in the collection
  Set Page = frmDiceBar.FindPage(lisPageList.List(lisPageList.ListIndex))
  If Page Is Nothing Then
      MsgBox "Error: page not found!", vbOKOnly + vbCritical, "Manage Pages"
      Exit Sub
    End If
  If MsgBox("Are you sure you want to delete the " & Page.Label & " page?", vbYesNo + vbQuestion, "Manage Pages") <> vbYes Then Exit Sub
  ' remove it from the collection
  frmDiceBar.ptrPages.Remove Page.Label
  ' remove it from cmbPages
  frmDiceBar.cmbPages.RemoveItem lisPageList.ListIndex
  If frmDiceBar.lastPageIndex = lisPageList.ListIndex Then
      frmDiceBar.lastPageIndex = 0 ' jump to first page
    ElseIf frmDiceBar.lastPageIndex > lisPageList.ListIndex Then
      frmDiceBar.lastPageIndex = frmDiceBar.lastPageIndex - 1
    End If
  ' remove it from lisPageList
  lisPageList.RemoveItem lisPageList.ListIndex
  Set Page = Nothing
  Call frmEditPages_EnableDisable
End Sub

Private Sub cmdUp_Click()
  Dim Label As String
  Dim position As Integer
  position = lisPageList.ListIndex
  Label = lisPageList.List(position)
  ' remove from the lists at current position
  lisPageList.RemoveItem position
  frmDiceBar.cmbPages.RemoveItem position
  ' insert into the lists at new position
  lisPageList.AddItem Label, position - 1
  frmDiceBar.cmbPages.AddItem Label, position - 1
  ' move lastPageIndex if necessary
  If frmDiceBar.lastPageIndex = position Then
      frmDiceBar.lastPageIndex = position - 1 ' move with it
    ElseIf frmDiceBar.lastPageIndex = position - 1 Then
      frmDiceBar.lastPageIndex = position
    End If
  ' move the selection to follow it
  lisPageList.ListIndex = position - 1
  Call frmEditPages_EnableDisable
End Sub

Private Sub cmdDown_Click()
  Dim Label As String
  Dim position As Integer
  position = lisPageList.ListIndex
  Label = lisPageList.List(position)
  ' remove from the lists at current position
  lisPageList.RemoveItem position
  frmDiceBar.cmbPages.RemoveItem position
  ' insert into the lists at new position
  lisPageList.AddItem Label, position + 1
  frmDiceBar.cmbPages.AddItem Label, position + 1
  ' move lastPageIndex if necessary
  If frmDiceBar.lastPageIndex = position Then
      frmDiceBar.lastPageIndex = position + 1 ' move with it
    ElseIf frmDiceBar.lastPageIndex = position + 1 Then
      frmDiceBar.lastPageIndex = position
    End If
  ' move the selection to follow it
  lisPageList.ListIndex = position + 1
  Call frmEditPages_EnableDisable
End Sub

Private Sub cmdClose_Click()
  Me.Hide
End Sub

Private Function PromptForName(prompt As String, default As String) As String
  Dim name As String
  ' generate a default, non-conflicting name
  If default = "" Then
      name = "Page 1"
      While PageNameInList(name)
        name = "Page " & Val(Mid(name, 6)) + 1
        Wend
    Else
      name = default
    End If
  ' prompt the user for a name using the default
  Do
    name = InputBox(prompt, "Manage Pages", name)
    If PageNameInList(name) Then MsgBox "Page """ & name & """ already exists.", vbOKOnly + vbExclamation, "Manage Pages"
    Loop While name <> "" And PageNameInList(name)
  PromptForName = name
End Function

Private Function PageNameInList(name As String) As Boolean
  Dim i As Integer
  For i = 0 To lisPageList.ListCount - 1
    If UCase(lisPageList.List(i)) = UCase(name) Then
        PageNameInList = True
        Exit Function
      End If
    Next
  PageNameInList = False
End Function
