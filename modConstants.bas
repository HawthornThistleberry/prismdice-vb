Attribute VB_Name = "modConstants"
Public Const conAppName = "Prism Dice"

Public Const DICE_TOTAL = 0          ' normal dice (P=ignored)
Public Const DICE_TOTAL_HIGHEST = 10 ' total the highest rolls (P=how many)
Public Const DICE_TOTAL_LOWEST = 11  ' total the lowest rolls (P=how many)
Public Const DICE_DIGIT = 20         ' read off results as digits (P=0 or 1 for base)
Public Const DICE_OPEN_ENDED = 30    ' Rolemaster-style dice (P=size of reroll range)
Public Const DICE_OPEN_HIGH = 31     ' as above but high-open-ended only
Public Const DICE_OPEN_LOW = 32      ' as above but low-open-ended only
Public Const DICE_COUNT_UNDER = 40   ' count rolls under a value (P=the value)
Public Const DICE_COUNT_OVER = 41    ' count rolls over a value (P=the value)
Public Const DICE_TOTAL_UNDER = 50   ' total rolls under a value (P=the value)
Public Const DICE_TOTAL_OVER = 51    ' total rolls over a value (P=the value)
Public Const DICE_AVERAGING = 60     ' averaging dice (P=how many extreme values to lose)
Public Const DICE_FUDGE = 61         ' FUDGE-style dice (P=size of minus/plus range)
Public Const DICE_ARSMAGICA = 62     ' Ars Magica dice (P=number of botch dice, or 0 for quality die)
Public Const DICE_CHAMPIONS = 63     ' Champions dice (P=0 for killing, or number of rolls that count as 0/2 for stun)
Public Const DICE_EXALTED = 64       ' Exalted dice (P=threshold)
Public Const DICE_WEGWILD = 65       ' WEG Star Wars wild dice: some high-open-ended (P=how many open)
Public Const DICE_SERIES_COUNT = 70  ' count rolls until failure (P=roll under value)
Public Const DICE_DIVIDED = 80       ' divide one die roll by another (P=denominator size)

Public Const SORT_NONE = 0
Public Const SORT_ASCENDING = 1
Public Const SORT_DESCENDING = 2

Public Const MAXIMUM_LOG_LINES = 1000
