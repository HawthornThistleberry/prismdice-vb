VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsPage"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Public Label As String          ' name for the page
Public Buttons As New Collection    ' the actual dice settings for the buttons

Public Sub Create()
  Dim i As Integer, DieSize As Integer
  Me.Label = "Default Page"
  For i = 1 To 8
    If i <= 5 Then
        DieSize = i * 2 + 2
      ElseIf i = 6 Then
        DieSize = 20
      ElseIf i = 7 Then
        DieSize = 30
      Else
        DieSize = 100
      End If
    Dim NewButton As New clsDiceSetting
    NewButton.Create DieSize
    Buttons.Add NewButton, CStr(i)
    Set NewButton = Nothing
    Next
End Sub

Public Sub ShowPage()
  Dim i As Integer
  ' make sure the combo box points to the right one
  For i = 0 To frmDiceBar.cmbPages.ListCount - 1
    If frmDiceBar.cmbPages.List(i) = Label Then frmDiceBar.cmbPages.ListIndex = i
    Next
  frmDiceBar.lastPageIndex = frmDiceBar.cmbPages.ListIndex
  ' update all the buttons
  For i = 1 To 8
    Buttons.Item(i).ShowButton frmDiceBar.cmdDice(i - 1)
    Next
End Sub

Public Sub LoadFromRegistry(newLabel As String)
  Dim i As Integer
  Label = newLabel
  For i = 1 To 8
    Buttons.Item(i).LoadFromRegistry newLabel, i
    Next
End Sub

Public Sub SaveToRegistry()
  Dim i As Integer
  For i = 1 To 8
    Buttons.Item(i).SaveToRegistry Label, i
    Next
End Sub

Public Sub Export(filenum As Integer)
  Dim i As Integer
  For i = 1 To 8
    Buttons.Item(i).Export i, filenum
    Next
End Sub

Public Sub Import(filenum As Integer)
  Dim i As Integer
  For i = 1 To 8
    Buttons.Item(i).Import i, filenum
    Next
End Sub

