VERSION 5.00
Begin VB.Form frmDiceSettings 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Dice Settings"
   ClientHeight    =   4095
   ClientLeft      =   3360
   ClientTop       =   3015
   ClientWidth     =   6390
   HelpContextID   =   200
   Icon            =   "frmDiceSettings.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   273
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   426
   ShowInTaskbar   =   0   'False
   Begin VB.Frame fraDieWizard 
      Caption         =   "Die Wizard"
      Height          =   1095
      HelpContextID   =   210
      Left            =   3960
      TabIndex        =   43
      Top             =   0
      Width           =   2415
      Begin VB.CommandButton cmdWizard 
         Caption         =   "Set die magically!"
         Enabled         =   0   'False
         Height          =   375
         HelpContextID   =   210
         Left            =   120
         TabIndex        =   13
         Top             =   600
         Width           =   2175
      End
      Begin VB.ComboBox cmbWizard 
         Height          =   315
         HelpContextID   =   210
         ItemData        =   "frmDiceSettings.frx":0442
         Left            =   120
         List            =   "frmDiceSettings.frx":0482
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   240
         Width           =   2175
      End
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      HelpContextID   =   220
      Left            =   5520
      TabIndex        =   27
      Top             =   3720
      Width           =   855
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      HelpContextID   =   220
      Left            =   5520
      TabIndex        =   26
      Top             =   3240
      Width           =   855
   End
   Begin VB.Frame fraSortRolls 
      Caption         =   "Sort Rolls"
      Height          =   1095
      HelpContextID   =   220
      Left            =   3960
      TabIndex        =   38
      Top             =   3000
      Width           =   1455
      Begin VB.OptionButton optSortDescending 
         Caption         =   "Descending"
         Height          =   255
         HelpContextID   =   220
         Left            =   120
         TabIndex        =   25
         Top             =   720
         Width           =   1215
      End
      Begin VB.OptionButton optSortAscending 
         Caption         =   "Ascending"
         Height          =   255
         HelpContextID   =   220
         Left            =   120
         TabIndex        =   24
         Top             =   480
         Width           =   1215
      End
      Begin VB.OptionButton optSortNone 
         Caption         =   "None"
         Height          =   255
         HelpContextID   =   220
         Left            =   120
         TabIndex        =   23
         Top             =   240
         Value           =   -1  'True
         Width           =   1215
      End
   End
   Begin VB.Frame fraParameters 
      Caption         =   "Dice Parameters"
      Height          =   1215
      HelpContextID   =   220
      Left            =   0
      TabIndex        =   36
      Top             =   2880
      Width           =   3855
      Begin VB.CommandButton cmdDieParamPlus 
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "Webdings"
            Size            =   9.75
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         HelpContextID   =   220
         Left            =   3480
         TabIndex        =   11
         Top             =   840
         Width           =   255
      End
      Begin VB.CommandButton cmdDieParamMinus 
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "Webdings"
            Size            =   9.75
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         HelpContextID   =   220
         Left            =   3240
         TabIndex        =   10
         Top             =   840
         Width           =   255
      End
      Begin VB.TextBox txtDieParam 
         Height          =   285
         HelpContextID   =   220
         Left            =   120
         TabIndex        =   9
         Top             =   840
         Width           =   3135
      End
      Begin VB.Label lblParameterExplanation 
         Height          =   495
         Left            =   135
         TabIndex        =   37
         Top             =   240
         Width           =   3615
      End
   End
   Begin VB.Frame fraModifiers 
      Caption         =   "Modify Rolls"
      Height          =   1695
      HelpContextID   =   220
      Left            =   3960
      TabIndex        =   33
      Top             =   1200
      Width           =   2415
      Begin VB.CommandButton cmdTotalMultiplierPlus 
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "Webdings"
            Size            =   9.75
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         HelpContextID   =   220
         Left            =   2040
         TabIndex        =   22
         Top             =   1320
         Width           =   255
      End
      Begin VB.CommandButton cmdTotalAdderPlus 
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "Webdings"
            Size            =   9.75
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         HelpContextID   =   220
         Left            =   2040
         TabIndex        =   19
         Top             =   1080
         Width           =   255
      End
      Begin VB.CommandButton cmdTotalMultiplierMinus 
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "Webdings"
            Size            =   9.75
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         HelpContextID   =   220
         Left            =   1800
         TabIndex        =   21
         Top             =   1320
         Width           =   255
      End
      Begin VB.CommandButton cmdTotalAdderMinus 
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "Webdings"
            Size            =   9.75
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         HelpContextID   =   220
         Left            =   1800
         TabIndex        =   18
         Top             =   1080
         Width           =   255
      End
      Begin VB.TextBox txtTotalMultiplier 
         Height          =   285
         HelpContextID   =   220
         Left            =   960
         MaxLength       =   4
         TabIndex        =   20
         Text            =   "1"
         Top             =   1320
         Width           =   855
      End
      Begin VB.TextBox txtTotalAdder 
         Height          =   285
         HelpContextID   =   220
         Left            =   960
         MaxLength       =   4
         TabIndex        =   17
         Text            =   "0"
         Top             =   1080
         Width           =   855
      End
      Begin VB.CommandButton cmdDieAdderPlus 
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "Webdings"
            Size            =   9.75
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         HelpContextID   =   220
         Left            =   2040
         TabIndex        =   16
         Top             =   480
         Width           =   255
      End
      Begin VB.CommandButton cmdDieAdderMinus 
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "Webdings"
            Size            =   9.75
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         HelpContextID   =   220
         Left            =   1800
         TabIndex        =   15
         Top             =   480
         Width           =   255
      End
      Begin VB.TextBox txtDieAdder 
         Height          =   285
         HelpContextID   =   220
         Left            =   960
         MaxLength       =   4
         TabIndex        =   14
         Text            =   "0"
         Top             =   480
         Width           =   855
      End
      Begin VB.Label lblTotalMultiplier 
         Alignment       =   1  'Right Justify
         Caption         =   "Multiply by"
         Height          =   255
         Left            =   120
         TabIndex        =   41
         Top             =   1350
         Width           =   735
      End
      Begin VB.Label lblTotalAdder 
         Alignment       =   1  'Right Justify
         Caption         =   "Add"
         Height          =   255
         Left            =   120
         TabIndex        =   40
         Top             =   1110
         Width           =   735
      End
      Begin VB.Label lblDieAdder 
         Alignment       =   1  'Right Justify
         Caption         =   "Add"
         Height          =   255
         Left            =   120
         TabIndex        =   39
         Top             =   510
         Width           =   735
      End
      Begin VB.Label lblToIndividualDice 
         Caption         =   "To each individual die:"
         Height          =   255
         Left            =   120
         TabIndex        =   35
         Top             =   240
         Width           =   2055
      End
      Begin VB.Label lblToTotal 
         Caption         =   "To the total result:"
         Height          =   255
         Left            =   120
         TabIndex        =   34
         Top             =   840
         Width           =   1575
      End
   End
   Begin VB.Frame fraBasics 
      Caption         =   "Basics"
      Height          =   2055
      HelpContextID   =   220
      Left            =   0
      TabIndex        =   29
      Top             =   720
      Width           =   3855
      Begin VB.CommandButton cmdDieSizePlus 
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "Webdings"
            Size            =   9.75
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         HelpContextID   =   220
         Left            =   3480
         TabIndex        =   7
         Top             =   600
         Width           =   255
      End
      Begin VB.CommandButton cmdDieSizeMinus 
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "Webdings"
            Size            =   9.75
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         HelpContextID   =   220
         Left            =   3240
         TabIndex        =   6
         Top             =   600
         Width           =   255
      End
      Begin VB.CommandButton cmdNumDicePlus 
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "Webdings"
            Size            =   9.75
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         HelpContextID   =   220
         Left            =   3480
         TabIndex        =   4
         Top             =   240
         Width           =   255
      End
      Begin VB.CommandButton cmdNumDiceMinus 
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "Webdings"
            Size            =   9.75
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         HelpContextID   =   220
         Left            =   3240
         TabIndex        =   3
         Top             =   240
         Width           =   255
      End
      Begin VB.ComboBox cmbDieSize 
         Height          =   315
         HelpContextID   =   220
         ItemData        =   "frmDiceSettings.frx":0577
         Left            =   1440
         List            =   "frmDiceSettings.frx":0593
         TabIndex        =   5
         Text            =   "20"
         Top             =   600
         Width           =   1830
      End
      Begin VB.ComboBox cmbDieType 
         Height          =   315
         HelpContextID   =   230
         ItemData        =   "frmDiceSettings.frx":05B5
         Left            =   1440
         List            =   "frmDiceSettings.frx":0604
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   960
         Width           =   2295
      End
      Begin VB.TextBox txtNumDice 
         Height          =   285
         HelpContextID   =   220
         Left            =   1440
         MaxLength       =   4
         TabIndex        =   2
         Text            =   "1"
         Top             =   240
         Width           =   1815
      End
      Begin VB.Label lblDieTypeExplanation 
         Height          =   615
         Left            =   120
         TabIndex        =   42
         Top             =   1320
         Width           =   3615
      End
      Begin VB.Label lblDieSize 
         Alignment       =   1  'Right Justify
         Caption         =   "Die Size:"
         Height          =   255
         Left            =   120
         TabIndex        =   32
         Top             =   645
         Width           =   1215
      End
      Begin VB.Label lblDieType 
         Alignment       =   1  'Right Justify
         Caption         =   "Die Type:"
         Height          =   255
         Left            =   120
         TabIndex        =   31
         Top             =   1020
         Width           =   1215
      End
      Begin VB.Label lblNumDice 
         Alignment       =   1  'Right Justify
         Caption         =   "Number of Dice:"
         Height          =   255
         Left            =   120
         TabIndex        =   30
         Top             =   285
         Width           =   1215
      End
   End
   Begin VB.Frame fraButtonLabel 
      Caption         =   "Button Label"
      Height          =   615
      HelpContextID   =   220
      Left            =   0
      TabIndex        =   28
      Top             =   0
      Width           =   3855
      Begin VB.CommandButton cmdAutoLabel 
         Caption         =   "Automatic"
         Height          =   300
         HelpContextID   =   220
         Left            =   2760
         TabIndex        =   1
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox txtLabel 
         Height          =   285
         HelpContextID   =   220
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Width           =   2655
      End
   End
End
Attribute VB_Name = "frmDiceSettings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const FIELD_NUMDICE_LOW = 1
Const FIELD_NUMDICE_HIGH = 2
Const FIELD_DIESIZE_LOW = 3
Const FIELD_DIESIZE_HIGH = 4
Const FIELD_DIEPARAM_LOW = 5
Const FIELD_DIEPARAM_HIGH = 6
Const FIELD_DIEADDER_LOW = 7
Const FIELD_DIEADDER_HIGH = 8

Private Sub Form_Load()
  ' center the form on the screen
  Left = (Screen.Width - Width) / 2
  Top = (Screen.Height - Height) / 2
End Sub

Private Sub Form_Activate()
  If txtNumDice.Enabled Then
      txtNumDice.SetFocus
    Else
      cmbDieSize.SetFocus
    End If
  cmbWizard.ListIndex = -1
  Tag = "0"
End Sub

Private Sub cmdOK_Click()
  Me.Tag = "1"
  Me.Hide
End Sub

Private Sub cmdCancel_Click()
  Me.Hide
End Sub

Private Function ValueLimit(Field As Integer) As Integer
  Dim DieType As Integer
  DieType = cmbDieType.ItemData(cmbDieType.ListIndex)
  ' calculates a limit for a given field, based on the
  ' values in all the other fields
    
  '               Num     Size   Param          DieAdder  Other
  ' TOTAL         1,9999  2,inf  disable        -inf,inf
  ' TOTAL_HIGHEST 2,9999  2,inf  1,N-1          -inf,inf
  ' TOTAL_LOWEST  2,9999  2,inf  1,N-1          -inf,inf
  ' DIGIT         2,12    2,10   0,if(S=10,0,1) 0,9-S     disable TAdd, TMult
  ' OPEN_ENDED    1,9999  2,inf  1,up(S/2)-1    -inf,inf
  ' OPEN_HIGH     1,9999  2,inf  1,up(S/2)-1    -inf,inf
  ' OPEN_LOW      1,9999  2,inf  1,up(S/2)-1    -inf,inf
  ' COUNT_UNDER   1,9999  4,inf  2,S-1          -S+1,S-1
  ' COUNT_OVER    1,9999  4,inf  2,S-1          -S+1,S-1
  ' TOTAL_UNDER   1,9999  4,inf  2,S-1          -inf,inf
  ' TOTAL_OVER    1,9999  4,inf  2,S-1          -inf,inf
  ' AVERAGING     1,9999  4,100  1,rnd(S/3)     -inf,inf
  ' FUDGE         1,9999  4,inf  1,up(S/2)-1    -inf,inf
  ' ARSMAGICA     disable 4,inf  0,9999         -inf,inf
  ' CHAMPIONS     1,9999  4,inf  0,up(S/2)-1    -inf,inf
  ' WEGWILD       1,9999  2,inf  0,2            -inf,inf
  ' SERIES_COUNT  1,9999  2,inf  1,S-1          disable
  ' DIVIDED       1,9999  2,inf  2,int(S/2)     0,inf
  ' EXALTED       1,9999  2,inf  2,S-1          -inf,inf
  Select Case Field
    Case FIELD_NUMDICE_LOW
      ValueLimit = 1
      If DieType = DICE_DIGIT Or DieType = DICE_TOTAL_LOWEST Or _
        DieType = DICE_TOTAL_HIGHEST Then ValueLimit = 2
    Case FIELD_NUMDICE_HIGH
      ValueLimit = 9999
      If DieType = DICE_DIGIT Then ValueLimit = 12
    Case FIELD_DIESIZE_LOW
      ValueLimit = 2
      If DieType = DICE_COUNT_UNDER Or DieType = DICE_COUNT_OVER Or _
          DieType = DICE_TOTAL_UNDER Or DieType = DICE_TOTAL_OVER Or _
          DieType = DICE_FUDGE Or DieType = DICE_CHAMPIONS Or _
          DieType = DICE_AVERAGING Then _
          ValueLimit = 4
    Case FIELD_DIESIZE_HIGH
      ValueLimit = 30000
      If DieType = DICE_DIGIT Then ValueLimit = 10
      If DieType = DICE_AVERAGING Then ValueLimit = 100
    Case FIELD_DIEPARAM_LOW
      ValueLimit = 1
      If DieType = DICE_DIGIT Or DieType = DICE_TOTAL Or DieType = DICE_WEGWILD Or _
          DieType = DICE_ARSMAGICA Or DieType = DICE_CHAMPIONS Then ValueLimit = 0
      If DieType = DICE_COUNT_UNDER Or DieType = DICE_COUNT_OVER Or _
          DieType = DICE_TOTAL_UNDER Or DieType = DICE_TOTAL_OVER Or _
          DieType = DICE_EXALTED Or DieType = DICE_DIVIDED Then ValueLimit = 2
    Case FIELD_DIEPARAM_HIGH
      ValueLimit = Val(cmbDieSize) - 1
      If DieType = DICE_TOTAL_HIGHEST Or DieType = DICE_TOTAL_LOWEST Or DieType = DICE_WEGWILD Then _
          ValueLimit = Val(txtNumDice) - 1
      If DieType = DICE_DIGIT Then _
          ValueLimit = IIf(Val(cmbDieSize) = 10, 0, 1)
      If DieType = DICE_OPEN_ENDED Or DieType = DICE_OPEN_HIGH Or _
          DieType = DICE_OPEN_LOW Or DieType = DICE_FUDGE Or _
          DieType = DICE_CHAMPIONS Then _
          ValueLimit = Int(Val(cmbDieSize) / 2 + 0.9999) - 1
      If DieType = DICE_ARSMAGICA Then ValueLimit = 9999
      If DieType = DICE_ARSMAGICA Then ValueLimit = 2
      If DieType = DICE_AVERAGING Then ValueLimit = Int(Val(cmbDieSize) / 3 + 0.5)
      If DieType = DICE_DIVIDED Then ValueLimit = Int(Val(cmbDieSize) / 2)
      If DieType = DICE_TOTAL Then ValueLimit = 9999
    Case FIELD_DIEADDER_LOW
      ValueLimit = -30000
      If DieType = DICE_DIGIT Or DieType = DICE_DIVIDED Then _
          ValueLimit = 0
      If DieType = DICE_COUNT_UNDER Or DieType = DICE_COUNT_OVER Then _
          ValueLimit = -1 * Val(cmbDieSize) + 1
    Case FIELD_DIEADDER_HIGH
      ValueLimit = 30000
      If DieType = DICE_DIGIT Then _
          ValueLimit = IIf(Val(cmbDieSize) > 9, 0, 9 - Val(cmbDieSize))
      If DieType = DICE_COUNT_UNDER Or DieType = DICE_COUNT_OVER Then _
          ValueLimit = Val(cmbDieSize) - 1
    End Select
End Function

Private Function OutOfLimits(Field As Integer) As Boolean
  Select Case Field
    Case FIELD_NUMDICE_LOW, FIELD_NUMDICE_HIGH
      OutOfLimits = Val(txtNumDice) < ValueLimit(FIELD_NUMDICE_LOW) Or _
        Val(txtNumDice) > ValueLimit(FIELD_NUMDICE_HIGH)
    Case FIELD_DIESIZE_LOW, FIELD_DIESIZE_HIGH
      OutOfLimits = Val(cmbDieSize) < ValueLimit(FIELD_DIESIZE_LOW) Or _
        Val(cmbDieSize) > ValueLimit(FIELD_DIESIZE_HIGH)
    Case FIELD_DIEPARAM_LOW, FIELD_DIEPARAM_HIGH
      OutOfLimits = Val(txtDieParam) < ValueLimit(FIELD_DIEPARAM_LOW) Or _
        Val(txtDieParam) > ValueLimit(FIELD_DIEPARAM_HIGH)
    Case FIELD_DIEADDER_LOW, FIELD_DIEADDER_HIGH
      OutOfLimits = Val(txtDieAdder) < ValueLimit(FIELD_DIEADDER_LOW) Or _
        Val(txtDieAdder) > ValueLimit(FIELD_DIEADDER_HIGH)
    End Select
End Function

Private Sub CheckFieldForLimit(ByRef action As Integer, Field As Integer)
  If Not OutOfLimits(Field) Then Exit Sub
  If action = -1 Then
      action = MsgBox("One or more fields are out of limits for this die type.  Should I reset those fields to be within limits?  (If you answer No, I'll change the dice type to Total.)", vbYesNo + vbQuestion, "Dice Settings")
    End If
  If action = vbNo Then
      cmbDieType.ListIndex = 0
    End If
  Select Case Field
    Case FIELD_NUMDICE_LOW, FIELD_NUMDICE_HIGH
      If Val(txtNumDice) < ValueLimit(FIELD_NUMDICE_LOW) Then txtNumDice = CStr(ValueLimit(FIELD_NUMDICE_LOW))
      If Val(txtNumDice) > ValueLimit(FIELD_NUMDICE_HIGH) Then txtNumDice = CStr(ValueLimit(FIELD_NUMDICE_HIGH))
    Case FIELD_DIESIZE_LOW, FIELD_DIESIZE_HIGH
      If Val(cmbDieSize) < ValueLimit(FIELD_DIESIZE_LOW) Then cmbDieSize = CStr(ValueLimit(FIELD_DIESIZE_LOW))
      If Val(cmbDieSize) > ValueLimit(FIELD_DIESIZE_HIGH) Then cmbDieSize = CStr(ValueLimit(FIELD_DIESIZE_HIGH))
    Case FIELD_DIEPARAM_LOW, FIELD_DIEPARAM_HIGH
      If Val(txtDieParam) < ValueLimit(FIELD_DIEPARAM_LOW) Then txtDieParam = CStr(ValueLimit(FIELD_DIEPARAM_LOW))
      If Val(txtDieParam) > ValueLimit(FIELD_DIEPARAM_HIGH) Then txtDieParam = CStr(ValueLimit(FIELD_DIEPARAM_HIGH))
    Case FIELD_DIEADDER_LOW, FIELD_DIEADDER_HIGH
      If Val(txtDieAdder) < ValueLimit(FIELD_DIEADDER_LOW) Then txtDieAdder = CStr(ValueLimit(FIELD_DIEADDER_LOW))
      If Val(txtDieAdder) > ValueLimit(FIELD_DIEADDER_HIGH) Then txtDieAdder = CStr(ValueLimit(FIELD_DIEADDER_HIGH))
    End Select
End Sub

Private Sub cmbDieType_Click()
  Dim action As Integer
  
  ' most fields are always enabled
  txtNumDice.Enabled = True
  cmdNumDiceMinus.Enabled = True
  cmdNumDicePlus.Enabled = True
  txtDieParam.Enabled = True
  cmdDieParamMinus.Enabled = True
  cmdDieParamPlus.Enabled = True
  txtTotalAdder.Enabled = True
  cmdTotalAdderMinus.Enabled = True
  cmdTotalAdderPlus.Enabled = True
  txtTotalMultiplier.Enabled = True
  cmdTotalMultiplierMinus.Enabled = True
  cmdTotalMultiplierPlus.Enabled = True
  optSortNone.Enabled = True
  optSortAscending.Enabled = True
  optSortDescending.Enabled = True
  
  ' based on the type we're setting, set limits
  ' and explanatory texts, and disable fields if needed
  Select Case cmbDieType.ItemData(cmbDieType.ListIndex)
    Case DICE_TOTAL          ' normal dice (P=ignored)
      lblDieTypeExplanation = "Roll the dice and add them up."
      lblParameterExplanation = "No parameters for this type of dice."
      txtDieParam.Enabled = False
      cmdDieParamMinus.Enabled = False
      cmdDieParamPlus.Enabled = False
    Case DICE_TOTAL_HIGHEST  ' total the highest rolls (P=how many)
      lblDieTypeExplanation = "Roll the dice and add up the highest few."
      lblParameterExplanation = "Specify how many dice to add to the total.  Should be less than the number of dice rolled."
      optSortNone.Enabled = False
      optSortAscending.Enabled = False
      optSortDescending.Enabled = False
      optSortDescending = True
    Case DICE_TOTAL_LOWEST   ' total the lowest rolls (P=how many)
      lblDieTypeExplanation = "Roll the dice and add up the lowest few."
      lblParameterExplanation = "Specify how many dice to add to the total.  Should be less than the number of dice rolled."
      optSortNone.Enabled = False
      optSortAscending.Enabled = False
      optSortDescending.Enabled = False
      optSortAscending = True
    Case DICE_DIGIT          ' read off results as digits (P=0 or 1 for base)
      lblDieTypeExplanation = "Count off the rolls as the digits of a number, the way 2d10 is read as a d%."
      lblParameterExplanation = "Specify 0 to make each die count up from 0 (as with d%), or 1 to count up from 1 (as in Toon)."
      txtTotalAdder.Enabled = False
      cmdTotalAdderMinus.Enabled = False
      cmdTotalAdderPlus.Enabled = False
      txtTotalMultiplier.Enabled = False
      cmdTotalMultiplierMinus.Enabled = False
      cmdTotalMultiplierPlus.Enabled = False
    Case DICE_OPEN_ENDED     ' Rolemaster-style dice (P=size of reroll range)
      lblDieTypeExplanation = "For each die, roll, and if the roll is over or under a certain value, reroll and add or subtract."
      lblParameterExplanation = "How many roll values lead to a reroll?"
    Case DICE_OPEN_HIGH      ' as above but high-open-ended only
      lblDieTypeExplanation = "For each die, roll, and if the roll is over a certain value, reroll and add."
      lblParameterExplanation = "How many roll values lead to a reroll?"
    Case DICE_OPEN_LOW       ' as above but low-open-ended only
      lblDieTypeExplanation = "For each die, roll, and if the roll is under a certain value, reroll and subtract."
      lblParameterExplanation = "How many roll values lead to a reroll?"
    Case DICE_COUNT_UNDER    ' count rolls under a value (P=the value)
      lblDieTypeExplanation = "Count how many rolls are under a certain value."
      lblParameterExplanation = "Enter the number the rolls need to be under, to be counted."
    Case DICE_COUNT_OVER     ' count rolls over a value (P=the value)
      lblDieTypeExplanation = "Count how many rolls are over a certain value."
      lblParameterExplanation = "Enter the number the rolls need to be over, to be counted."
    Case DICE_TOTAL_UNDER    ' total rolls under a value (P=the value)
      lblDieTypeExplanation = "Total all of the rolls that are under a certain value."
      lblParameterExplanation = "Enter the number the rolls need to be under, to be totaled."
    Case DICE_TOTAL_OVER     ' total rolls over a value (P=the value)
      lblDieTypeExplanation = "Total all of the rolls that are over a certain value."
      lblParameterExplanation = "Enter the number the rolls need to be over, to be totaled."
    Case DICE_AVERAGING      ' averaging dice (P=how tightly averaged)
      lblDieTypeExplanation = "Dice that implement a bell curve by changing which numbers are on the sides.  For instance, averaging d6s are labelled 2, 3, 3, 4, 4, 5."
      lblParameterExplanation = "How many extreme values should be replaced with average values?"
    Case DICE_FUDGE          ' FUDGE-style dice (P=size of minus/plus range)
      lblDieTypeExplanation = "Replace sides of dice with -1, 0, and +1."
      lblParameterExplanation = "Specify how many sides of each die are treated as -1 (the same number are treated as +1)."
    Case DICE_ARSMAGICA      ' Ars Magica dice (P=1 for quality, 2 for stress)
      lblDieTypeExplanation = "Rolls of 1 cause a reroll, doubled.  0 rolls might require botch dice."
      lblParameterExplanation = "How many botch dice to roll?  (0 for quality dice, usually 1 for stress dice but sometimes more.)"
      txtNumDice = "1"
      txtNumDice.Enabled = False
      cmdNumDiceMinus.Enabled = False
      cmdNumDicePlus.Enabled = False
      optSortNone.Enabled = False
      optSortAscending.Enabled = False
      optSortDescending.Enabled = False
      optSortNone = True
    Case DICE_CHAMPIONS      ' Champions dice (P=0 for killing, or number of rolls that count as 0/2 for stun)
      lblDieTypeExplanation = "Can do stunning or killing attacks; result shows points of stun and body damage."
      lblParameterExplanation = "0 for killing attacks; otherwise, number of sides treated as 0 or 2 body damage (usually 1)."
    Case DICE_EXALTED        ' Exalted dice (P=threshold)
      lblDieTypeExplanation = "Typically you use d10s with a threshold of 7 for successes in Exalted."
      lblParameterExplanation = "Threshold for successes (rolls with a 1 and no successes are critical failures)."
    Case DICE_WEGWILD        ' WEG SW Wild dice (P=how many wild)
      lblDieTypeExplanation = "WEG's Star Wars usually uses d6s, one of which is wild."
      lblParameterExplanation = "Wild die of 1 means: 0=Highlight only; 1=remove it and highest roll; 2=low-open-ended."
    Case DICE_SERIES_COUNT   ' count rolls until failure (P=roll under value)
      lblDieTypeExplanation = "Keep rolling as long as the roll is under a given value, then count the rolls."
      lblParameterExplanation = "Specify the value you have to roll under, to keep rolling and counting."
    Case DICE_DIVIDED        ' divide one die roll by another (P=denominator size)
      lblDieTypeExplanation = "Roll pairs of dice and divide one of them by the other."
      lblParameterExplanation = "Specify the size of the denominator die (the die on the bottom of the fraction)."
    End Select
  
  ' Check all the values and see if any are outside the limits
  action = -1 ' no selection chosen yet
  CheckFieldForLimit action, FIELD_NUMDICE_LOW
  CheckFieldForLimit action, FIELD_DIESIZE_LOW
  CheckFieldForLimit action, FIELD_DIEPARAM_LOW
  CheckFieldForLimit action, FIELD_DIEADDER_LOW
End Sub

Private Sub txtLabel_GotFocus()
  Call SelectField(txtLabel)
End Sub

Private Sub txtLabel_Change()
  Dim i As Integer, j As Integer
  i = 0
  Do
    i = InStr(i + 1, Me.Caption, "(")
    If i <> 0 Then j = i
    Loop While i <> 0
  Me.Caption = Left(Me.Caption, j) & txtLabel & ")"
End Sub

Private Sub cmdAutoLabel_Click()
  Dim result As String
  result = "d" & cmbDieSize
  'If cmbDieSize = "100" Then result = "d%"
  If txtDieAdder <> "0" Then result = "(" & result & _
    IIf(Val(txtDieAdder) < 0, "", "+") & txtDieAdder & ")"
  If txtNumDice <> "1" Then result = txtNumDice & result
  If cmbDieSize = "100" Then result = Left(result, Len(result) - 3) & "%"
  Select Case cmbDieType.ItemData(cmbDieType.ListIndex)
    Case DICE_TOTAL_HIGHEST
      result = result & " highest " & txtDieParam
    Case DICE_TOTAL_LOWEST
      result = result & " lowest " & txtDieParam
    Case DICE_DIGIT
      result = result & " digit dice"
    Case DICE_OPEN_ENDED
      result = result & " open-ended"
      If Not ((cmbDieSize = "100" And txtDieParam = "5") Or _
        (cmbDieSize = "20" And txtDieParam = "1")) Then _
        result = result & "(" & txtDieParam & ")"
    Case DICE_OPEN_HIGH
      result = result & " high-open-ended"
      If Not ((cmbDieSize = "100" And txtDieParam = "5") Or _
        (cmbDieSize = "20" And txtDieParam = "1")) Then _
        result = result & "(" & txtDieParam & ")"
    Case DICE_OPEN_LOW
      result = result & " low-open-ended"
      If Not ((cmbDieSize = "100" And txtDieParam = "5") Or _
        (cmbDieSize = "20" And txtDieParam = "1")) Then _
        result = result & "(" & txtDieParam & ")"
    Case DICE_COUNT_UNDER
      result = result & " count under " & txtDieParam
    Case DICE_COUNT_OVER
      result = result & " count over " & txtDieParam
    Case DICE_TOTAL_UNDER
      result = result & " total under " & txtDieParam
    Case DICE_TOTAL_OVER
      result = result & " total over " & txtDieParam
    Case DICE_AVERAGING
      result = result & " averaging dice"
    Case DICE_FUDGE
      result = "FUDGE " & result & " dice"
    Case DICE_ARSMAGICA
      If txtDieParam = 0 Then
          result = "Ars Magica quality"
        ElseIf txtDieParam = 1 Then
          result = "Ars Magica stress"
        Else
          result = "Ars Magica stress (" & txtDieParam & " botch)"
        End If
    Case DICE_CHAMPIONS
      If txtDieParam = 0 Then
          result = "Champions killing"
        ElseIf txtDieParam = 1 Then
          result = "Champions stunning"
        Else
          result = "Champions stunning (" & txtDieParam & " sides)"
        End If
    Case DICE_EXALTED
      If txtDieParam = 7 And cmbDieSize = 10 Then
          result = "Exalted (" & txtNumDice & ")"
        Else
          result = "Exalted " & result & ":" & txtDieParam
        End If
    Case DICE_WEGWILD
      If cmbDieSize = 6 Then
          result = "WEG Wild (" & txtNumDice & ")"
        Else
          result = "WEG Wild " & txtNumDice & "d" & cmbDieSize
        End If
      Select Case txtDieParam
        Case 1: result = result & " (drop)"
        Case 2: result = result & " (low)"
        End Select
    Case DICE_SERIES_COUNT
      result = result & " series under " & txtDieParam
    Case DICE_DIVIDED
      If txtNumDice = "1" Then
          result = "d" & cmbDieSize & "/d" & txtDieParam
        Else
          result = txtNumDice & "(d" & cmbDieSize & "/d" & txtDieParam & ")"
        End If
    End Select
  If txtTotalMultiplier <> 1 Then result = result & " * " & txtTotalMultiplier
  If txtTotalAdder <> 0 Then result = result & " + " & txtTotalAdder
  txtLabel = result
End Sub

Private Sub txtNumDice_GotFocus()
  Call SelectField(txtNumDice)
End Sub

Private Sub txtNumDice_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub cmdNumDiceMinus_Click()
  Dim action As Integer
  If Val(txtNumDice) <= ValueLimit(FIELD_NUMDICE_LOW) Then
      Beep
      txtNumDice = CStr(ValueLimit(FIELD_NUMDICE_LOW))
      Exit Sub
    End If
  txtNumDice = CStr(Val(txtNumDice) - 1)
  action = vbYes
  CheckFieldForLimit action, FIELD_DIEPARAM_LOW
End Sub

Private Sub cmdNumDicePlus_Click()
  Dim action As Integer
  If Val(txtNumDice) >= ValueLimit(FIELD_NUMDICE_HIGH) Then
      Beep
      txtNumDice = CStr(ValueLimit(FIELD_NUMDICE_HIGH))
      Exit Sub
    End If
  txtNumDice = CStr(Val(txtNumDice) + 1)
  action = vbYes
  CheckFieldForLimit action, FIELD_DIEPARAM_LOW
End Sub

Private Sub txtNumDice_LostFocus()
  Dim action As Integer
  If OutOfLimits(FIELD_NUMDICE_LOW) Then
      MsgBox "This value is inappropriate for this die type.", vbOKOnly + vbExclamation, "Dice Settings"
      txtNumDice = CStr(ValueLimit(FIELD_NUMDICE_LOW))
    End If
  action = vbYes
  CheckFieldForLimit action, FIELD_DIEPARAM_LOW
End Sub

Private Sub cmbDieSize_GotFocus()
  Call SelectField(cmbDieSize)
End Sub

Private Sub cmbDieSize_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub cmdDieSizeMinus_Click()
  Dim action As Integer
  If Val(cmbDieSize) <= ValueLimit(FIELD_DIESIZE_LOW) Then
      Beep
      cmbDieSize = CStr(ValueLimit(FIELD_DIESIZE_LOW))
      Exit Sub
    End If
  cmbDieSize = CStr(Val(cmbDieSize) - 1)
  action = vbYes
  CheckFieldForLimit action, FIELD_DIEPARAM_LOW
End Sub

Private Sub cmdDieSizePlus_Click()
  Dim action As Integer
  If Val(cmbDieSize) >= ValueLimit(FIELD_DIESIZE_HIGH) Then
      Beep
      cmbDieSize = CStr(ValueLimit(FIELD_DIESIZE_HIGH))
      Exit Sub
    End If
  cmbDieSize = CStr(Val(cmbDieSize) + 1)
  action = vbYes
  CheckFieldForLimit action, FIELD_DIEPARAM_LOW
End Sub

Private Sub cmbDieSize_LostFocus()
  Dim action As Integer
  If OutOfLimits(FIELD_DIESIZE_LOW) Then
      MsgBox "This value is inappropriate for this die type.", vbOKOnly + vbExclamation, "Dice Settings"
      cmbDieSize = CStr(ValueLimit(FIELD_DIESIZE_LOW))
    End If
  action = vbYes
  CheckFieldForLimit action, FIELD_DIEPARAM_LOW
End Sub

Private Sub txtDieParam_GotFocus()
  Call SelectField(txtDieParam)
End Sub

Private Sub txtDieParam_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub cmdDieParamMinus_Click()
  If Val(txtDieParam) <= ValueLimit(FIELD_DIEPARAM_LOW) Then
      Beep
      txtDieParam = CStr(ValueLimit(FIELD_DIEPARAM_LOW))
      Exit Sub
    End If
  txtDieParam = CStr(Val(txtDieParam) - 1)
End Sub

Private Sub cmdDieParamPlus_Click()
  If Val(txtDieParam) >= ValueLimit(FIELD_DIEPARAM_HIGH) Then
      Beep
      txtDieParam = CStr(ValueLimit(FIELD_DIEPARAM_HIGH))
      Exit Sub
    End If
  txtDieParam = CStr(Val(txtDieParam) + 1)
End Sub

Private Sub txtDieParam_LostFocus()
  If OutOfLimits(FIELD_DIEPARAM_LOW) Then
      MsgBox "This value is inappropriate for this die type.", vbOKOnly + vbExclamation, "Dice Settings"
      txtDieParam = CStr(ValueLimit(FIELD_DIEPARAM_LOW))
    End If
End Sub

Private Sub txtDieAdder_GotFocus()
  Call SelectField(txtDieAdder)
End Sub

Private Sub txtDieAdder_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii, txtDieAdder)
End Sub

Private Sub cmdDieAdderMinus_Click()
  If Val(txtDieAdder) <= ValueLimit(FIELD_DIEADDER_LOW) Then
      Beep
      txtDieAdder = CStr(ValueLimit(FIELD_DIEADDER_LOW))
      Exit Sub
    End If
  txtDieAdder = CStr(Val(txtDieAdder) - 1)
End Sub

Private Sub cmdDieAdderPlus_Click()
  If Val(txtDieAdder) >= ValueLimit(FIELD_DIEADDER_HIGH) Then
      Beep
      txtDieAdder = CStr(ValueLimit(FIELD_DIEADDER_HIGH))
      Exit Sub
    End If
  txtDieAdder = CStr(Val(txtDieAdder) + 1)
End Sub

Private Sub txtDieAdder_LostFocus()
  If OutOfLimits(FIELD_DIEADDER_LOW) Then
      MsgBox "This value is inappropriate for this die type.", vbOKOnly + vbExclamation, "Dice Settings"
      txtDieAdder = CStr(ValueLimit(FIELD_DIEADDER_LOW))
    End If
End Sub

Private Sub txtTotalAdder_GotFocus()
  Call SelectField(txtTotalAdder)
End Sub

Private Sub txtTotalAdder_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii, txtTotalAdder)
End Sub

Private Sub cmdTotalAdderMinus_Click()
  txtTotalAdder = CStr(Val(txtTotalAdder) - 1)
End Sub

Private Sub cmdTotalAdderPlus_Click()
  txtTotalAdder = CStr(Val(txtTotalAdder) + 1)
End Sub

Private Sub txtTotalMultiplier_GotFocus()
  Call SelectField(txtTotalMultiplier)
End Sub

Private Sub txtTotalMultiplier_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub cmdTotalMultiplierMinus_Click()
  If Val(txtTotalMultiplier) <= 1 Then
      Beep
      txtTotalMultiplier = "1"
      Exit Sub
    End If
  txtTotalMultiplier = CStr(Val(txtTotalMultiplier) - 1)
End Sub

Private Sub cmdTotalMultiplierPlus_Click()
  txtTotalMultiplier = CStr(Val(txtTotalMultiplier) + 1)
End Sub

Public Sub SelectField(o As Object)
  o.SelStart = 0
  o.SelLength = Len(o)
End Sub

Public Function OnlyDigits(ByVal KeyAscii As Integer) As Integer
  OnlyDigits = KeyAscii
  If InStr("0123456789", Chr$(KeyAscii)) = 0 And _
      KeyAscii <> 8 And KeyAscii <> 127 Then
      OnlyDigits = 0
      Beep
    End If
End Function

Public Function OnlyDigitsAndMinus(ByVal KeyAscii As Integer, o As Object) As Integer
  OnlyDigitsAndMinus = KeyAscii
  If (o.SelStart <> 0 Or (Chr$(KeyAscii) <> "-" And Chr$(KeyAscii) <> "+")) And _
      InStr("0123456789", Chr$(KeyAscii)) = 0 And _
      KeyAscii <> 8 And KeyAscii <> 127 Then
      OnlyDigitsAndMinus = 0
      Beep
    End If
End Function

Private Sub SetDieType(DieType As Integer)
  Dim i As Integer
  For i = 1 To cmbDieType.ListCount
    If cmbDieType.ItemData(i - 1) = DieType Then
        cmbDieType.ListIndex = i - 1
        Exit For
      End If
    Next
End Sub

Private Sub cmbWizard_Click()
  cmdWizard.Enabled = True
End Sub

Private Sub cmdWizard_Click()
  SetDieType DICE_TOTAL
  Select Case cmbWizard.ListIndex
    Case 0 ' Prism          1d20 open-ended range 1
      txtNumDice = "1"
      cmbDieSize = "20"
      txtDieParam = "1"
      txtDieAdder = "0"
      SetDieType DICE_OPEN_ENDED
      txtTotalAdder = "0"
      txtTotalMultiplier = "1"
      txtLabel = "Prism"
    Case 1 ' Prism Stat     5d20 FUDGE range 7
      txtNumDice = "5"
      cmbDieSize = "20"
      txtDieParam = "7"
      txtDieAdder = "0"
      SetDieType DICE_FUDGE
      txtTotalAdder = "0"
      txtTotalMultiplier = "1"
      txtLabel = "Prism Stat"
    Case 2 ' Rolemaster     1d100 open-ended range 5
      txtNumDice = "1"
      cmbDieSize = "100"
      txtDieParam = "5"
      txtDieAdder = "0"
      SetDieType DICE_OPEN_ENDED
      txtTotalAdder = "0"
      txtTotalMultiplier = "1"
      txtLabel = "Rolemaster"
    Case 3 ' RM High        1d100 open-high range 5
      txtNumDice = "1"
      cmbDieSize = "100"
      txtDieParam = "5"
      txtDieAdder = "0"
      SetDieType DICE_OPEN_HIGH
      txtTotalAdder = "0"
      txtTotalMultiplier = "1"
      txtLabel = "Rolemaster High"
    Case 4 ' RM Low         1d100 open-low range 5
      txtNumDice = "1"
      cmbDieSize = "100"
      txtDieParam = "5"
      txtDieAdder = "0"
      SetDieType DICE_OPEN_LOW
      txtTotalAdder = "0"
      txtTotalMultiplier = "1"
      txtLabel = "Rolemaster Low"
    Case 5 ' GURPS          3d6
      txtNumDice = "3"
      cmbDieSize = "6"
      txtDieParam = "1"
      txtDieAdder = "0"
      SetDieType DICE_TOTAL
      txtTotalAdder = "0"
      txtTotalMultiplier = "1"
      txtLabel = "GURPS"
    Case 6 ' FUDGE          3d6 FUDGE range 2
      txtNumDice = "4"
      cmbDieSize = "6"
      txtDieParam = "2"
      txtDieAdder = "0"
      SetDieType DICE_FUDGE
      txtTotalAdder = "0"
      txtTotalMultiplier = "1"
      txtLabel = "FUDGE"
    Case 7 ' In Nomine      3d6 Digit Dice base=1
      txtNumDice = "3"
      cmbDieSize = "6"
      txtDieParam = "1"
      txtDieAdder = "0"
      SetDieType DICE_DIGIT
      txtTotalAdder = "0"
      txtTotalMultiplier = "1"
      txtLabel = "In Nomine"
    Case 8 ' Toon           2d6 Digit Dice base=1
      txtNumDice = "2"
      cmbDieSize = "6"
      txtDieParam = "1"
      txtDieAdder = "0"
      SetDieType DICE_DIGIT
      txtTotalAdder = "0"
      txtTotalMultiplier = "1"
      txtLabel = "Toon"
    Case 9 ' AD&D Stats     3d6
      txtNumDice = "3"
      cmbDieSize = "6"
      txtDieParam = "1"
      txtDieAdder = "0"
      SetDieType DICE_TOTAL
      txtTotalAdder = "0"
      txtTotalMultiplier = "1"
      txtLabel = "AD&D Stats"
    Case 10 ' AD&D Cheater  4d6 total highest 3
      txtNumDice = "4"
      cmbDieSize = "6"
      txtDieParam = "3"
      txtDieAdder = "0"
      SetDieType DICE_TOTAL_HIGHEST
      txtTotalAdder = "0"
      txtTotalMultiplier = "1"
      txtLabel = "D&D3e Stats"
    Case 11 ' Averaging d6  1d6 averaging 1
      txtNumDice = "1"
      cmbDieSize = "6"
      txtDieParam = "1"
      txtDieAdder = "0"
      SetDieType DICE_AVERAGING
      txtTotalAdder = "0"
      txtTotalMultiplier = "1"
      txtLabel = "Averaging Die"
    Case 12 ' Trinity/Aeon  6d10 count over 7
      txtNumDice = "6"
      cmbDieSize = "10"
      txtDieParam = "7"
      txtDieAdder = "0"
      SetDieType DICE_COUNT_OVER
      txtTotalAdder = "0"
      txtTotalMultiplier = "1"
      txtLabel = "Trinity/Aeon"
      optSortAscending = True
    Case 13 ' Ars Magica stress
      txtNumDice = "1"
      cmbDieSize = "10"
      txtDieParam = "1"
      txtDieAdder = "0"
      SetDieType DICE_ARSMAGICA
      txtTotalAdder = "0"
      txtTotalMultiplier = "1"
      txtLabel = "Ars Magica stress"
    Case 14 ' Ars Magica quality
      txtNumDice = "1"
      cmbDieSize = "10"
      txtDieParam = "0"
      txtDieAdder = "0"
      SetDieType DICE_ARSMAGICA
      txtTotalAdder = "0"
      txtTotalMultiplier = "1"
      txtLabel = "Ars Magica quality"
    Case 15 ' Champions stunning
      txtNumDice = "1"
      cmbDieSize = "6"
      txtDieParam = "1"
      txtDieAdder = "0"
      SetDieType DICE_CHAMPIONS
      txtTotalAdder = "0"
      txtTotalMultiplier = "1"
      txtLabel = "Champions stunning"
    Case 16 ' Champions killing
      txtNumDice = "1"
      cmbDieSize = "6"
      txtDieParam = "0"
      txtDieAdder = "0"
      SetDieType DICE_CHAMPIONS
      txtTotalAdder = "0"
      txtTotalMultiplier = "1"
      txtLabel = "Champions killing"
    Case 17 ' Exalted
      cmbDieSize = "10"
      txtDieParam = "7"
      txtDieAdder = "0"
      SetDieType DICE_EXALTED
      txtTotalAdder = "0"
      txtTotalMultiplier = "1"
      txtLabel = "Exalted"
    Case 18 ' WEG Wild
      txtNumDice = "3"
      cmbDieSize = "6"
      txtDieParam = "1"
      txtDieAdder = "0"
      SetDieType DICE_WEGWILD
      txtTotalAdder = "0"
      txtTotalMultiplier = "1"
      txtLabel = "WEG Wild"
    Case 19 ' d6-d6 which is really 2d6-7
      txtNumDice = "2"
      cmbDieSize = "6"
      txtDieParam = "0"
      txtDieAdder = "0"
      SetDieType DICE_TOTAL
      txtTotalAdder = "-7"
      txtTotalMultiplier = "1"
      txtLabel = "d6-d6"
    End Select
End Sub

