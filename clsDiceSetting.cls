VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsDiceSetting"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' This file contains the data structures for the
' dice settings data that drives the whole program.

' WEG Wild Dice changes
' - change to a single die type; always assume only a single wild die
' - the parameter determines how to handle a 1 roll.  Options are:
'   - 0: do nothing special, just add it in, but flag it
'   - 1: remove it and the highest die from the total
'   - 2: let it be low-open-ended (as current BiWild dice)

Option Explicit

Public Label As String             ' text to appear on die
Public NumDice As Integer          ' roll this many dice
Public DieType As Integer          ' see constants above
Public DieSize As Integer          ' number of sides of dice
Public DieParam As Integer         ' parameter P mentioned above
Public DieAdder As Integer         ' add to each die
Public TotalAdder As Integer       ' add to total
Public TotalMultiplier As Integer  ' multiply total by this
Public sorttype As Integer         ' should individual results be sorted?

Public Sub Create(size As Integer)
  If size = 100 Then
      Label = "d%"
    Else
      Label = "d" & Trim(Str(size))
    End If
  NumDice = 1
  DieType = DICE_TOTAL
  DieSize = size
  DieParam = 1
  DieAdder = 0
  TotalAdder = 0
  TotalMultiplier = 1
  sorttype = SORT_NONE
End Sub

Public Sub ShowButton(Button As Object)
  Button.Caption = frmDiceBar.DoubleAmpersands(Label)
End Sub

Public Sub LoadFromRegistry(PageLabel As String, i As Integer)
  Label = GetSetting(conAppName, PageLabel, Trim(Str(i)) & "Label", 1)
  NumDice = GetSetting(conAppName, PageLabel, Trim(Str(i)) & "NumDice", 1)
  DieType = GetSetting(conAppName, PageLabel, Trim(Str(i)) & "DieType", DICE_TOTAL)
  DieSize = GetSetting(conAppName, PageLabel, Trim(Str(i)) & "DieSize", 6)
  DieParam = GetSetting(conAppName, PageLabel, Trim(Str(i)) & "DieParam", 0)
  DieAdder = GetSetting(conAppName, PageLabel, Trim(Str(i)) & "DieAdder", 0)
  TotalAdder = GetSetting(conAppName, PageLabel, Trim(Str(i)) & "TotalAdder", 0)
  TotalMultiplier = GetSetting(conAppName, PageLabel, Trim(Str(i)) & "TotalMultiplier", 1)
  sorttype = GetSetting(conAppName, PageLabel, Trim(Str(i)) & "SortType", SORT_NONE)
End Sub

Public Sub SaveToRegistry(PageLabel As String, i As Integer)
  Call SaveSetting(conAppName, PageLabel, Trim(Str(i)) & "Label", Label)
  Call SaveSetting(conAppName, PageLabel, Trim(Str(i)) & "NumDice", NumDice)
  Call SaveSetting(conAppName, PageLabel, Trim(Str(i)) & "DieType", DieType)
  Call SaveSetting(conAppName, PageLabel, Trim(Str(i)) & "DieSize", DieSize)
  Call SaveSetting(conAppName, PageLabel, Trim(Str(i)) & "DieParam", DieParam)
  Call SaveSetting(conAppName, PageLabel, Trim(Str(i)) & "DieAdder", DieAdder)
  Call SaveSetting(conAppName, PageLabel, Trim(Str(i)) & "TotalAdder", TotalAdder)
  Call SaveSetting(conAppName, PageLabel, Trim(Str(i)) & "TotalMultiplier", TotalMultiplier)
  Call SaveSetting(conAppName, PageLabel, Trim(Str(i)) & "SortType", sorttype)
End Sub

Public Sub Export(i As Integer, filenum As Integer)
  Write #filenum, "  Button", i, Label
  Write #filenum, "    NumDice", NumDice
  Write #filenum, "    DieType", DieType
  Write #filenum, "    DieSize", DieSize
  Write #filenum, "    DieParam", DieParam
  Write #filenum, "    DieAdder", DieAdder
  Write #filenum, "    TotalAdder", TotalAdder
  Write #filenum, "    TotalMultiplier", TotalMultiplier
  Write #filenum, "    SortType", sorttype
End Sub

Public Sub Import(i As Integer, filenum As Integer)
  Dim s As String, j As Integer
  Input #filenum, s, j, Label
  Input #filenum, s, NumDice
  Input #filenum, s, DieType
  Input #filenum, s, DieSize
  Input #filenum, s, DieParam
  Input #filenum, s, DieAdder
  Input #filenum, s, TotalAdder
  Input #filenum, s, TotalMultiplier
  Input #filenum, s, sorttype
End Sub

Public Function Roll(ByRef details As String) As String
  Dim result As Integer, thisroll As Integer
  Dim thisresult As Integer, i As Integer
  Dim rstring As String
  Dim rollarray(9999) As Integer
  Dim rolldetails(9999) As String
  Dim Distribution(100) As Integer

  Randomize 'seed the random number generator
  details = ""
  result = 0
  rstring = ""
  thisroll = 1
  
  Select Case DieType
    
    Case DICE_TOTAL          ' normal dice (P=ignored)
      For i = 1 To NumDice
        rollarray(i) = Int(DieSize * Rnd) + 1 + DieAdder
        rolldetails(i) = Str(rollarray(i))
        result = result + rollarray(i)
        Next i
      If sorttype <> SORT_NONE Then SortRolls rollarray, rolldetails, NumDice, sorttype
      For i = 1 To NumDice
        details = details & rolldetails(i) & ", "
        Next i
      Roll = Trim(Str(result * TotalMultiplier + TotalAdder))

    Case DICE_TOTAL_HIGHEST, DICE_TOTAL_LOWEST
      For i = 1 To NumDice
        rollarray(i) = Int(DieSize * Rnd) + 1 + DieAdder
        rolldetails(i) = Str(rollarray(i))
        Next i
      SortRolls rollarray, rolldetails, NumDice, IIf(DieType = DICE_TOTAL_LOWEST, SORT_ASCENDING, SORT_DESCENDING)
      For i = 1 To DieParam
        details = details & rollarray(i) & ", "
        result = result + rollarray(i)
        Next i
      details = Trim(details) & " ["
      For i = DieParam + 1 To NumDice
        details = details & rollarray(i) & " "
        Next i
      details = Trim(details) & "]"
      Roll = Trim(Str(result * TotalMultiplier + TotalAdder))
    
    Case DICE_DIGIT          ' read off results as digits (P=0 or 1 for base)
      For i = 1 To NumDice
        rollarray(i) = Int(DieSize * Rnd) + DieParam + DieAdder
        rolldetails(i) = Str(rollarray(i))
        Next i
      If sorttype <> SORT_NONE Then SortRolls rollarray, rolldetails, NumDice, sorttype
      For i = 1 To NumDice
        details = details & rolldetails(i) & ", "
        rstring = rstring & Trim(Str(rollarray(i)))
        Next i
      Roll = rstring
    
    ' Die adders and multipliers are applied after open-endedness is
    ' determined (so a +5 on an open-ended d% doesn't make a 92 go open).
    Case DICE_OPEN_ENDED     ' Rolemaster-style dice (P=size of reroll range)
      For i = 1 To NumDice
        thisroll = Int(DieSize * Rnd) + 1
        thisresult = thisroll
        rolldetails(i) = rolldetails(i) & thisroll
        If thisroll <= DieParam Then
            Do
              thisroll = Int(DieSize * Rnd) + 1
              rolldetails(i) = rolldetails(i) & "-" & thisroll
              thisresult = thisresult - thisroll
              Loop While thisroll > DieSize - DieParam
            rolldetails(i) = rolldetails(i) & "=" & Trim(Str(thisresult))
          ElseIf thisroll > DieSize - DieParam Then
            Do
              thisroll = Int(DieSize * Rnd) + 1
              rolldetails(i) = rolldetails(i) & "+" & thisroll
              thisresult = thisresult + thisroll
              Loop While thisroll > DieSize - DieParam
            rolldetails(i) = rolldetails(i) & "=" & Trim(Str(thisresult))
          Else
            rolldetails(i) = Trim(rolldetails(i))
          End If
        rollarray(i) = result + thisresult + DieAdder
        Next i
      If sorttype <> SORT_NONE Then SortRolls rollarray, rolldetails, NumDice, sorttype
      For i = 1 To NumDice
        result = result + rollarray(i)
        details = details & rolldetails(i) & ", "
        Next i
      Roll = Trim(Str(result * TotalMultiplier + TotalAdder))
    Case DICE_OPEN_HIGH      ' as above but high-open-ended only
      For i = 1 To NumDice
        thisroll = Int(DieSize * Rnd) + 1
        thisresult = thisroll
        rolldetails(i) = rolldetails(i) & thisroll
        If thisroll > DieSize - DieParam Then
            Do
              thisroll = Int(DieSize * Rnd) + 1
              rolldetails(i) = rolldetails(i) & "+" & thisroll
              thisresult = thisresult + thisroll
              Loop While thisroll > DieSize - DieParam
            rolldetails(i) = rolldetails(i) & "=" & Trim(Str(thisresult))
          Else
            rolldetails(i) = Trim(rolldetails(i))
          End If
        result = result + thisresult + DieAdder
        Next i
      If sorttype <> SORT_NONE Then SortRolls rollarray, rolldetails, NumDice, sorttype
      For i = 1 To NumDice
        result = result + rollarray(i)
        details = details & rolldetails(i) & ", "
        Next i
      Roll = Trim(Str(result * TotalMultiplier + TotalAdder))
    Case DICE_OPEN_LOW       ' as above but low-open-ended only
      For i = 1 To NumDice
        thisroll = Int(DieSize * Rnd) + 1
        thisresult = thisroll
        rolldetails(i) = rolldetails(i) & thisroll
        If thisroll <= DieParam Then
            Do
              thisroll = Int(DieSize * Rnd) + 1
              rolldetails(i) = rolldetails(i) & "-" & thisroll
              thisresult = thisresult - thisroll
              Loop While thisroll > DieSize - DieParam
            rolldetails(i) = rolldetails(i) & "=" & Trim(Str(thisresult))
          Else
            rolldetails(i) = Trim(rolldetails(i))
          End If
        result = result + thisresult + DieAdder
        Next i
      If sorttype <> SORT_NONE Then SortRolls rollarray, rolldetails, NumDice, sorttype
      For i = 1 To NumDice
        result = result + rollarray(i)
        details = details & rolldetails(i) & ", "
        Next i
      Roll = Trim(Str(result * TotalMultiplier + TotalAdder))
    
    ' Dice adders are applied before comparison to the limit on
    ' count under, count over, total under, and total over rolls.
    Case DICE_COUNT_UNDER    ' count rolls under a value (P=the value)
      For i = 1 To NumDice
        rollarray(i) = Int(DieSize * Rnd) + 1
        If rollarray(i) < DieParam Then
            rolldetails(i) = rollarray(i)
            result = result + 1 + DieAdder
          Else
            rolldetails(i) = "[" & rollarray(i) & "]"
          End If
        Next i
      If sorttype <> SORT_NONE Then SortRolls rollarray, rolldetails, NumDice, sorttype
      For i = 1 To NumDice
        details = details & rolldetails(i) & ", "
        Next i
      Roll = Trim(Str(result * TotalMultiplier + TotalAdder))
    Case DICE_COUNT_OVER     ' count rolls over a value (P=the value)
      For i = 1 To NumDice
        rollarray(i) = Int(DieSize * Rnd) + 1
        If rollarray(i) >= DieParam Then
            rolldetails(i) = rollarray(i)
            result = result + 1 + DieAdder
          Else
            rolldetails(i) = "[" & rollarray(i) & "]"
          End If
        Next i
      If sorttype <> SORT_NONE Then SortRolls rollarray, rolldetails, NumDice, sorttype
      For i = 1 To NumDice
        details = details & rolldetails(i) & ", "
        Next i
      Roll = Trim(Str(result * TotalMultiplier + TotalAdder))
    Case DICE_TOTAL_UNDER    ' total rolls under a value (P=the value)
      result = 0
      For i = 1 To NumDice
        rollarray(i) = Int(DieSize * Rnd) + 1
        If rollarray(i) < DieParam Then
            rolldetails(i) = rollarray(i)
            result = result + rollarray(i) + DieAdder
          Else
            rolldetails(i) = "[" & rollarray(i) & "]"
          End If
        Next i
      If sorttype <> SORT_NONE Then SortRolls rollarray, rolldetails, NumDice, sorttype
      For i = 1 To NumDice
        details = details & rolldetails(i) & ", "
        Next i
      Roll = Trim(Str(result * TotalMultiplier + TotalAdder))
    Case DICE_TOTAL_OVER     ' total rolls over a value (P=the value)
      For i = 1 To NumDice
        rollarray(i) = Int(DieSize * Rnd) + 1
        If rollarray(i) >= DieParam Then
            rolldetails(i) = rollarray(i)
            result = result + rollarray(i) + DieAdder
          Else
            rolldetails(i) = "[" & rollarray(i) & "]"
          End If
        Next i
      If sorttype <> SORT_NONE Then SortRolls rollarray, rolldetails, NumDice, sorttype
      For i = 1 To NumDice
        details = details & rolldetails(i) & ", "
        Next i
      Roll = Trim(Str(result * TotalMultiplier + TotalAdder))
    
    ' Die adders are applied before divided dice are divided, to both rolls.
    Case DICE_AVERAGING      ' averaging dice (P=how tightly averaged)
      BuildAveragingDice Distribution, Val(DieSize), Val(DieParam)
      ' The distribution should be done now; roll the dice
      For i = 1 To NumDice
        rollarray(i) = Distribution(Int(DieSize * Rnd) + 1) + DieAdder
        rolldetails(i) = Str(rollarray(i))
        result = result + rollarray(i)
        Next i
      If sorttype <> SORT_NONE Then SortRolls rollarray, rolldetails, NumDice, sorttype
      For i = 1 To NumDice
        details = details & rolldetails(i) & ", "
        Next i
      Roll = Trim(Str(result * TotalMultiplier + TotalAdder))
    
    ' Die adders are added to FUDGE dice only after the FUDGEiness has
    ' been implemented (e.g., d6 P1 standard FUDGE dice with +1 die
    ' adder yield results 0, +1, and +2 equally often).
    Case DICE_FUDGE          ' FUDGE-style dice (P=size of minus/plus range)
      For i = 1 To NumDice
        rollarray(i) = Int(DieSize * Rnd) + 1
        If rollarray(i) <= DieParam Then
            rolldetails(i) = "-"
            result = result - 1
          ElseIf rollarray(i) > DieSize - DieParam Then
            rolldetails(i) = "+"
            result = result + 1
          Else
            rolldetails(i) = "0"
          End If
        result = result + DieAdder
        Next i
      If sorttype <> SORT_NONE Then SortRolls rollarray, rolldetails, NumDice, sorttype
      For i = 1 To NumDice
        details = details & rolldetails(i) & " "
        Next i
      result = result * TotalMultiplier + TotalAdder
      Roll = IIf(result > 0, "+", "") & Trim(Str(result))
      details = Trim(details)

    Case DICE_ARSMAGICA      ' Ars Magica dice (P=number of botch dice, 0 for quality dice)
      thisroll = Int(DieSize * Rnd)
      If thisroll = 1 Then ' critical success
          details = "1 "
          i = 2
          thisroll = Int(DieSize * Rnd) + 1
          While thisroll = 1
            i = i * 2
            details = details & "1 "
            thisroll = Int(DieSize * Rnd) + 1
            Wend
          details = details & thisroll
          result = i * thisroll
          Roll = Trim(Str((result + DieAdder) * TotalMultiplier + TotalAdder))
        ElseIf thisroll = 0 And DieParam > 0 Then ' possible botch
          details = "0 ["
          result = 0
          For i = 1 To DieParam
            thisroll = Int(DieSize * Rnd)
            details = details & thisroll & " "
            If thisroll = 0 Then result = result + 1
            Next i
          details = Trim(details) & "]"
          If result = 0 Then
              Roll = Trim(Str(DieAdder * TotalMultiplier + TotalAdder))
            ElseIf result = 1 Then
              Roll = "Botch!"
            Else
              Roll = "Botch! (" & result & ")"
            End If
        ElseIf thisroll = 0 And DieParam = 0 Then ' 0 on quality is 10
          details = "10"
          Roll = "10"
        Else
          details = thisroll
          Roll = Trim(Str((thisroll + DieAdder) * TotalMultiplier + TotalAdder))
        End If
      details = Trim(details)

    Case DICE_CHAMPIONS      ' Champions dice (P=0 for killing, or number of rolls that count as 0/2 for stun)
      thisresult = 0 ' stun damage
      result = 0 ' body damage
      For i = 1 To NumDice
        rollarray(i) = Int(DieSize * Rnd) + 1 + DieAdder
        rolldetails(i) = Str(rollarray(i))
        If DieParam = 0 Then ' killing attack
            result = result + rollarray(i)
          Else ' stunning attack
            thisresult = thisresult + rollarray(i)
            If rollarray(i) <= DieParam Then ' counts as 0
                ' nothing happens
              ElseIf rollarray(i) >= DieSize - DieParam + 1 Then ' counts as 2
                result = result + 2
              Else ' all others
                result = result + 1
              End If
          End If
        Next i
      If sorttype <> SORT_NONE Then SortRolls rollarray, rolldetails, NumDice, sorttype
      For i = 1 To NumDice
        details = details & rolldetails(i) & ", "
        Next i
      If DieParam = 0 Then ' calculate stun die multiplier
          thisroll = Int(6 * Rnd) ' d6-1
          thisresult = result * thisroll
          details = details & " [stun mult " & thisroll & "]"
        End If
      Roll = Trim(Str(result * TotalMultiplier + TotalAdder)) & " b, " & _
             Trim(Str(thisresult * TotalMultiplier + TotalAdder)) & " s"

    Case DICE_EXALTED        ' Exalted (P=threshold)
      For i = 1 To NumDice
        rollarray(i) = Int(DieSize * Rnd) + 1
        If rollarray(i) >= DieParam Then
            rolldetails(i) = rollarray(i)
            result = result + 1 + DieAdder
            If rollarray(i) = DieSize Then result = result + 1  ' count 10s twice
          Else
            rolldetails(i) = "[" & rollarray(i) & "]"
          End If
        Next i
      If sorttype <> SORT_NONE Then SortRolls rollarray, rolldetails, NumDice, sorttype
      For i = 1 To NumDice
        details = details & rolldetails(i) & ", "
        Next i
      Roll = Trim(Str(result * TotalMultiplier + TotalAdder))
      If Roll = "0" Then ' if no successes and any of the rolls is 1, it's a crit failure
          For i = 1 To NumDice
            If rollarray(i) = 1 Then Roll = "Fumble!"
            Next i
        End If
      
    Case DICE_WEGWILD          ' WEG Wild (P=number wild)
      result = 0
      ' First roll the non-wild dice
      For i = 1 To NumDice - 1
        rollarray(i) = Int(DieSize * Rnd) + 1 + DieAdder
        rolldetails(i) = Str(rollarray(i))
        result = result + rollarray(i)
        Next i
      If sorttype <> SORT_NONE Then SortRolls rollarray, rolldetails, NumDice - 1, sorttype
      For i = 1 To NumDice - 1
        details = details & rolldetails(i) & ", "
        Next i
      ' Now roll the wild die
      thisroll = Int(DieSize * Rnd) + 1
      thisresult = thisroll
      details = details & "(" & thisroll
      If thisroll = 1 Then
          details = details & "*"
          Select Case DieParam
            ' Case 0 is to do nothing, since the * is already there
            Case 1:
              details = details & "- 1 - "
              thisroll = rollarray(1)
              For i = 2 To NumDice - 1
                If rollarray(i) > thisroll Then thisroll = rollarray(i)
                Next
              thisresult = thisresult - 1 - thisroll
              details = details & thisroll & "=" & Trim(Str(thisresult))
            Case 2:
              Do
                thisroll = Int(DieSize * Rnd) + 1
                details = details & "-" & thisroll
                thisresult = thisresult - thisroll
                Loop While thisroll = DieSize
              details = details & "=" & Trim(Str(thisresult))
            End Select
        ElseIf thisroll = DieSize Then
          Do
            thisroll = Int(DieSize * Rnd) + 1
            details = details & "+" & thisroll
            thisresult = thisresult + thisroll
            Loop While thisroll = DieSize
          details = details & "=" & Trim(Str(thisresult))
        End If
      result = result + thisresult + DieAdder
      details = details & ")"
      Roll = Trim(Str(result * TotalMultiplier + TotalAdder))
      
    Case DICE_SERIES_COUNT   ' count rolls until failure (P=roll under value)
      For i = 1 To NumDice
        thisresult = 0
        rolldetails(i) = ""
        Do
          thisroll = Int(DieSize * Rnd) + 1 + DieAdder
          rolldetails(i) = rolldetails(i) & thisroll & " "
          thisresult = thisresult + 1
          Loop While thisroll <= DieParam
        thisresult = thisresult - 1
        rolldetails(i) = Trim(rolldetails(i)) & "=" & thisresult
        rollarray(i) = thisresult
        result = result + thisresult
        Next i
      If sorttype <> SORT_NONE Then SortRolls rollarray, rolldetails, NumDice, sorttype
      For i = 1 To NumDice
        details = details & rolldetails(i) & ", "
        Next i
      Roll = Trim(Str(result * TotalMultiplier + TotalAdder))

    Case DICE_DIVIDED        ' divide one die roll by another (P=denominator size)
      For i = 1 To NumDice
        thisroll = Int(DieSize * Rnd) + 1 + DieAdder
        thisresult = Int(DieParam * Rnd) + 1 + DieAdder
        rollarray(i) = Int(thisroll / thisresult + 0.5)
        rolldetails(i) = thisroll & "/" & thisresult & "=" & rollarray(i)
        result = result + rollarray(i)
        Next i
      If sorttype <> SORT_NONE Then SortRolls rollarray, rolldetails, NumDice, sorttype
      For i = 1 To NumDice
        details = details & rolldetails(i) & ", "
        Next i
      Roll = Trim(Str(result * TotalMultiplier + TotalAdder))

    End Select
  details = Trim(details)
  If Right(details, 1) = "," Then details = Left(details, Len(details) - 1)
End Function

Private Sub SortRolls(rollarray() As Integer, details() As String, size As Integer, sorttype As Integer)
  Dim i As Integer, j As Integer, swapper As Integer, lowest As Integer
  Dim swapstring As String
  For i = 1 To size - 1
    lowest = i
    For j = i + 1 To size
      If (rollarray(j) < rollarray(lowest) And sorttype = SORT_ASCENDING) Or _
         (rollarray(j) > rollarray(lowest) And sorttype = SORT_DESCENDING) Then
          lowest = j
        End If
      Next j
    If lowest <> i Then
        swapper = rollarray(i)
        rollarray(i) = rollarray(lowest)
        rollarray(lowest) = swapper
        swapstring = details(i)
        details(i) = details(lowest)
        details(lowest) = swapstring
      End If
    Next i
End Sub

Private Sub BuildAveragingDice(Distribution() As Integer, size As Integer, param As Integer)
  Dim i As Integer, j As Integer
  Dim keysequence As Integer, rollover As Integer
  Dim leftinsert As Integer, rightinsert As Integer
  ' P=how many extreme values to remove and replace with central values.
  ' The algorithm for figuring out which values to add when removing
  ' these extreme values goes as follows:
  ' The key series is: 1 2  1 2 3  1 2 3 4  1 2 3 4 5  1 2 3 4 5 6  ...
  ' For each value taken off of the extreme ends (both ends),
  '   take the next value in the key series
  '   Go out from the center that many steps
  '     (e.g. on a d20, 1 step is values 10 and 11, 2 steps is 9 and 12, etc.)
  '   Add another copy of those two values
  '
  ' Thus, the progression of averaged d20s for various values of P is:
  ' P=01 (k=1): 02 03 04 05 06 07 08 09 10 10 11 11 12 13 14 15 16 17 18 19
  ' P=02 (k=2): 03 04 05 06 07 08 09 09 10 10 11 11 12 12 13 14 15 16 17 18
  ' P=03 (k=1): 04 05 06 07 08 09 09 10 10 10 11 11 11 12 12 13 14 15 16 17
  ' P=04 (k=2): 05 06 07 08 09 09 09 10 10 10 11 11 11 12 12 12 13 14 15 16
  ' P=05 (k=3): 06 07 08 08 09 09 09 10 10 10 11 11 11 12 12 12 13 13 14 15
  ' P=06 (k=1): 07 08 08 09 09 09 10 10 10 10 11 11 11 11 12 12 12 13 13 14
  ' P=07 (k=2): 08 08 09 09 09 09 10 10 10 10 11 11 11 11 12 12 12 12 13 13
  '
  ' This is as far as we could go.  P=08 would return the same result as P=07,
  ' and P=09 would be adding values that had completely disappeared from the
  ' series already.  I don't know if there's some way to tell what the upper
  ' limit is for P given a particular die size algorithmically short of the
  ' brute force method of actually generating the distribution.  Dividing the
  ' die size by 3 and rounding off seems to work.
  
  ' AVERAGING     1,9999  4,100  1,rnd(S/3)     -inf,inf
  
  ' load a flat distribution
  For i = 1 To size
    Distribution(i) = i
    Next i
  keysequence = 0 ' position in key sequence
  rollover = 2 ' next time key sequence will restart
  For i = 1 To param
    ' go to the next item in the key sequence
    keysequence = keysequence + 1
    If keysequence > rollover Then
        keysequence = 1
        rollover = rollover + 1
      End If
    ' find the "center" position
    If size Mod 2 = 0 Then ' even die size
        leftinsert = size / 2
        rightinsert = size / 2 + 1
      Else ' odd die size
        leftinsert = Int(size / 2) + 1
        rightinsert = Int(size / 2) + 1
      End If
    For j = 1 To keysequence - 1 ' move down to next value
      While leftinsert > 2 And Distribution(leftinsert - 1) = Distribution(leftinsert)
        leftinsert = leftinsert - 1
        Wend
      leftinsert = leftinsert - 1
      While rightinsert < size - 1 And Distribution(rightinsert + 1) = Distribution(rightinsert)
        rightinsert = rightinsert + 1
        Wend
      rightinsert = rightinsert + 1
      Next j
    ' open a space for the left insert
    For j = 1 To leftinsert - 1
      Distribution(j) = Distribution(j + 1)
      Next j
    ' the value that used to be there is right, so that's it
    ' open a space for the right insert
    For j = size To rightinsert + 1 Step -1
      Distribution(j) = Distribution(j - 1)
      Next j
    Next i
End Sub


